package com.ahmadrosid.inspections.fragment_view.presenter.home;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.view.View;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.DetailReportActivity;
import com.ahmadrosid.inspections.ui.RecyclerViewAdapter;
import com.ahmadrosid.inspections.api.ApiBuilder;
import com.ahmadrosid.inspections.api.ApiServices;
import com.ahmadrosid.inspections.api.model.ResponseInspections;
import com.ahmadrosid.inspections.data.Database;
import com.ahmadrosid.inspections.data.model.DBModelInspections;
import com.ahmadrosid.inspections.fragment_view.model.ItemInspections;
import com.ahmadrosid.inspections.fragment_view.view.home.FragmentInspectionView;
import com.ahmadrosid.inspections.helper.BitmapHelper;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.Logging;
import com.ahmadrosid.inspections.helper.NetworkHelper;
import com.ahmadrosid.inspections.helper.NotifHelper;
import com.ahmadrosid.inspections.helper.UploadHelper;
import com.ahmadrosid.inspections.holder.HolderInspections;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import id.zelory.compressor.FileUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ocittwo on 31/08/16.
 */
public class FragmentInspectionPresenter {

    private final FragmentInspectionView view;
    private RecyclerViewAdapter<DBModelInspections, HolderInspections> adapter;
    private final Context context;
    private FragmentInspectionPresenter presenter;

    public FragmentInspectionPresenter(Context context, FragmentInspectionView view) {
        this.context = context;
        this.view = view;
        presenter = this;
    }

    /**
     * Load data if have internet connection fetch fata from server if not fetch from local
     */
    public void loadData() {
        if (NetworkHelper.isNetworkConnected(context)) {
            loadDataFromServer();
        } else {
            displayedData();
        }
    }

    /**
     * Fetch data from local database
     */
    private void displayedData() {
        Database db = new Database(context);
        SQLiteDatabase database = db.getWritableDatabase();
        ArrayList<DBModelInspections> data_local = db.getAll(database);
        ArrayList<DBModelInspections> data_server = db.getAll(database);

        if (data_local.size() > data_server.size()) {
            Logging.log("Data server not updates!");
        } else if (data_server.size() > data_local.size()) {
            Logging.log("Data local not updated!");
        } else if (data_local.size() == data_server.size()) {
            Logging.log("All data is uptodate!");
        }

        /**
         * If have internet connections will display data from data server
         * If not local local database will displayed
         * And no data will displayed if dont have data on server or local
         */
        if (NetworkHelper.isNetworkConnected(context)) {
            displayData(data_server);
            if (data_server.size() < 0)
                view.showNoData();
        } else {
            if (data_local.size() < 0)
                view.showNoData();
            displayData(data_local);
        }
        view.finishRefresh();
    }

    /**
     * Fetch data from server
     * data will be saved to local databse using json database
     */
    private void loadDataFromServer() {
        final ArrayList<ItemInspections> listInspections = new ArrayList<>();
        ApiServices services = ApiBuilder.call();
        services.getInspections()
                .enqueue(new Callback<ResponseInspections>() {
                    @Override public void onResponse(Call<ResponseInspections> call, Response<ResponseInspections> response) {
                        if (response.body().isSuccess()){
                            //
                        }else{
                            view.showNoData();
                        }
                    }

                    @Override public void onFailure(Call<ResponseInspections> call, Throwable t) {

                    }
                });
    }

    /**
     * Save data from api to local database
     *
     * @param listInspections ArrayList
     */
    private void saveToLocalDatabase(ArrayList<ItemInspections> listInspections) {
        Logging.log("Save data from server to local database!");
        Database db = new Database(context);
        SQLiteDatabase database = db.getWritableDatabase();
        for (ItemInspections data : listInspections) {
            DBModelInspections local_data = new DBModelInspections(
                    data.getTitle(),
                    data.getPicture(),
                    data.getCompany(),
                    data.getDescription(),
                    data.getApproved(), null,
                    data.getCreated_at()
            );
            db.insertFromServer(database, local_data);
        }
    }

    /**
     * Displaying data into RecyclerView
     *
     * @param data ArrayList
     */
    private void displayData(ArrayList<DBModelInspections> data) {
        adapter = new RecyclerViewAdapter<DBModelInspections, HolderInspections>(
                data, DBModelInspections.class, R.layout.item_inspections, HolderInspections.class
        ) {
            @Override protected void executeViewHolder(HolderInspections viewHolder, final DBModelInspections model, int position) {
                viewHolder.bind(model, presenter);
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View view) {
                        context.startActivity(new Intent(context, DetailReportActivity.class).putExtra(Constants.CREATED_AT, model.created_at));
                    }
                });
            }
        };
        view.setListInspections(adapter);
    }

    /**
     * Save data report to server
     *
     * @param data_local DBModelInspections
     */
    public void uploadReportToServer(final DBModelInspections data_local) {
        NotifHelper.showNotifUpload(context);

        Uri uri = Uri.parse("file://" + data_local.picture);
        try {
            File file = FileUtil.from(context, uri);
            UploadHelper.postData(context, data_local, BitmapHelper.compressImage(context, file));
            Database database = new Database(context);
            SQLiteDatabase db = database.getWritableDatabase();
            database.editApproved(db, data_local.created_at);
        } catch (IOException e) {
            e.printStackTrace();
            NotifHelper.commonNotif(context, "Upload Failed!", "Failed compress images!");
        }
    }
}
