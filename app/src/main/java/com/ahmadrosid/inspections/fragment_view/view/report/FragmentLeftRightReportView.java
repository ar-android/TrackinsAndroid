package com.ahmadrosid.inspections.fragment_view.view.report;

import com.ahmadrosid.inspections.core.Presenter;
import com.ahmadrosid.inspections.ui.FormInputImage;

/**
 * Created by ocittwo on 10/9/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public interface FragmentLeftRightReportView extends Presenter.View{
    void pickImageFromGallery(int REQUEST_CODE);
    void pickImageFromCamera(int REQUEST_CODE);
    void setCurrentForm(FormInputImage formInputImage);
}
