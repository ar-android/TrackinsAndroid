package com.ahmadrosid.inspections.fragment_view.presenter.login;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.EditText;

import com.ahmadrosid.inspections.core.Presenter;
import com.ahmadrosid.inspections.data.data.UserProfile;
import com.ahmadrosid.inspections.fragment_view.fragment.login.FragmentRegister;
import com.ahmadrosid.inspections.fragment_view.view.login.FragmentRegisterView;
import com.ahmadrosid.inspections.helper.ProgressLoader;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | All Rights Reserved
 */
public class FragmentRegisterPresenter implements Presenter<FragmentRegisterView>{

    private FragmentRegister fragment;
    private FragmentRegisterView view;
    private Context context;

    public FragmentRegisterPresenter(FragmentRegister fragment) {
        this.fragment = fragment;
        this.context = fragment.getContext();
    }

    public void register(EditText input_username, EditText input_email, EditText input_password, EditText input_confirm_password) {
        if (validateInput(input_username, input_email, input_password, input_confirm_password)) {
            UserProfile.getInstance(context)
                    .setEmail(input_email.getText().toString())
                    .setUsername(input_username.getText().toString())
                    .setPassword(input_password.getText().toString())
                    .createUserProfile((email, username, password) -> sendToServer(username, email, password));
        }
    }

    private boolean validateInput(EditText username, EditText email, EditText password, EditText confirm_password) {
        if (checkEmpty(username)) {
            view.showMessage("Pleas input your username!");
            return false;
        } else if (checkEmpty(email)) {
            view.showMessage("Pleas input your email");
            return false;
        } else if (checkEmpty(password)) {
            view.showMessage("Please input your password!");
            return false;
        } else if (checkEmpty(confirm_password)) {
            view.showMessage("Please input confirm password!");
            return false;
        } else if (!password.getText().toString().equals(confirm_password.getText().toString())) {
            view.showMessage("Confirm password in correct!");
            return false;
        }
        return true;
    }

    private boolean checkEmpty(EditText v) {
        return TextUtils.isEmpty(v.getText().toString());
    }

    public void sendToServer(String username, String email, String password) {

        final ProgressLoader loader = new ProgressLoader(context);
        loader.showProgressDialog();
        new Handler().postDelayed(view::successRegister, 2500);
        loader.hideProgressDialog();


//        ApiServices register = ApiBuilder.call();
//        Log.d(TAG, "sendToServer: " + API_BASE_URL + "api/v1/register");
//        User user = new User(username, email, password);
//        register.register(user).enqueue(new Callback<ResponseRequest>() {
//            @Override public void onResponse(Call<ResponseRequest> call, Response<ResponseRequest> response) {
//                if (response.body().isSuccess()) {
//                    view.successRegister();
//                    loader.hideProgressDialog();
//                } else {
//                    loader.hideProgressDialog();
//                    view.showMessage(response.body().getMessage());
//                    Log.d(TAG, "onResponse: " + response.body().toString());
//                }
//            }
//
//            @Override public void onFailure(Call<ResponseRequest> call, Throwable t) {
//                Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
//                Log.d(TAG, "onFailure: " + t.toString());
//                view.showMessage("Failed register!");
//                loader.hideProgressDialog();
//            }
//        });
    }

    @Override public void onAttachView(FragmentRegisterView view) {
        this.view = view;
    }

    @Override public void onDetachView() {
        view = null;
    }
}
