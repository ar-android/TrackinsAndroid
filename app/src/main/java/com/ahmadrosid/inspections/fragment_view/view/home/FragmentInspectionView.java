package com.ahmadrosid.inspections.fragment_view.view.home;

import android.support.v7.widget.RecyclerView;

/**
 * Created by ocittwo on 31/08/16.
 */
public interface FragmentInspectionView {

    void setListInspections(RecyclerView.Adapter adapter);
    void showNoData();
    void finishRefresh();
}
