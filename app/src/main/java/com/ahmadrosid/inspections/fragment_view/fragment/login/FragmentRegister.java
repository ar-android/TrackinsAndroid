package com.ahmadrosid.inspections.fragment_view.fragment.login;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.view.LoginView;
import com.ahmadrosid.inspections.fragment_view.presenter.login.FragmentRegisterPresenter;
import com.ahmadrosid.inspections.fragment_view.view.login.FragmentRegisterView;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.SharedPref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | All Rights Reserved
 */
public class FragmentRegister extends Fragment implements FragmentRegisterView {

    private final LoginView loginView;
    private FragmentRegisterPresenter presenter;

    public FragmentRegister(LoginView loginView) {
        this.loginView = loginView;
    }

    @BindView(R.id.input_username) EditText input_username;
    @BindView(R.id.input_email) EditText input_email;
    @BindView(R.id.input_password) EditText input_password;
    @BindView(R.id.input_confirm_password) EditText input_confirm_password;
    @BindView(R.id.btn_register) Button btn_register;
    @BindView(R.id.login) Button btn_login;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        presenter = new FragmentRegisterPresenter(this);
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        setupView();
        return view;
    }

    private void setupView() {
        Typeface lato_light = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Light.ttf");
        input_username.setTypeface(lato_light);
        input_email.setTypeface(lato_light);
        input_password.setTypeface(lato_light);
        input_confirm_password.setTypeface(lato_light);
        btn_register.setTypeface(lato_light);
        btn_login.setTypeface(lato_light);

        String email = SharedPref.getString(Constants.User.EMAIL);
        String password = SharedPref.getString(Constants.User.PASSWORD);
        input_email.setText(email);
        input_password.setText(password);
    }

    @OnClick(R.id.btn_register) void clickRegister() {
        presenter.register(input_username, input_email, input_password, input_confirm_password);
    }

    @OnClick(R.id.login) void clickLogin() {
        loginView.setActivityFragment(new FragmentLogin(loginView));
    }

    @Override public void showMessage(String s) {
        Snackbar.make(btn_login, s, Snackbar.LENGTH_LONG).show();
    }

    @Override public void successRegister() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container_login, new FragmentSetImage(loginView))
                .commit();
    }

    @Override public void onStart() {
        super.onStart();
        presenter.onAttachView(this);
    }

    @Override public void onStop() {
        super.onStop();
        presenter.onDetachView();
    }

}
