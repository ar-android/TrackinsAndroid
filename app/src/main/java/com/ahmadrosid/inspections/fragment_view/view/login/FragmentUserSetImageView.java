package com.ahmadrosid.inspections.fragment_view.view.login;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public interface FragmentUserSetImageView {
    void showMessages(String s);
}
