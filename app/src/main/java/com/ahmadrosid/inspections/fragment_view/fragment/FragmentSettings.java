package com.ahmadrosid.inspections.fragment_view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.data.DataSettings;
import com.ahmadrosid.inspections.helper.SetToolbar;
import com.ahmadrosid.inspections.ui.ItemSettings;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ocittwo on 31/08/16.
 */
public class FragmentSettings extends Fragment{

    @BindView(R.id.item_settings_organization) ItemSettings organization;
    @BindView(R.id.item_settings_region) ItemSettings region;
    @BindView(R.id.item_settings_district) ItemSettings district;
    @BindView(R.id.item_settings_dealer_name) ItemSettings dealer_name;
    @BindView(R.id.item_settings_dealer_code) ItemSettings dealer_code;
    @BindView(R.id.item_settings_sales_name) ItemSettings sales_name;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SetToolbar.setTitleFragment(this, "Settings");
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setings, container, false);
        ButterKnife.bind(this, view);
        setupView();
        return view;
    }

    private void setupView() {
        DataSettings data = DataSettings.getInstance().get();
        organization.setItem(data.organization);
        region.setItem(data.region);
        district.setItem(data.district);
        dealer_name.setItem(data.dealer_name);
        dealer_code.setItem(data.dealer_code);
        sales_name.setItem(data.sales_name);
    }
}
