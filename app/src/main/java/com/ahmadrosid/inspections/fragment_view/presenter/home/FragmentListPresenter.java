package com.ahmadrosid.inspections.fragment_view.presenter.home;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.ui.RecyclerViewAdapter;
import com.ahmadrosid.inspections.fragment_view.fragment.FragmentList;
import com.ahmadrosid.inspections.holder.HolderListList;

import java.util.ArrayList;

/**
 * Created by ocittwo on 31/08/16.
 */
public class FragmentListPresenter {
    private final FragmentList fragment;
    private final Context context;
    private ArrayList<String> data;
    private RecyclerViewAdapter<String, HolderListList> adapter;

    public FragmentListPresenter(FragmentList fragment) {
        this.fragment = fragment;
        this.context = fragment.getContext();
    }

    public void setListList(RecyclerView listList) {
        data = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            data.add("Dataaa");
        }

        adapter = new RecyclerViewAdapter<String, HolderListList>(data, String.class, R.layout.item_list_list, HolderListList.class) {
            @Override protected void executeViewHolder(HolderListList viewHolder, String model, int position) {
                viewHolder.bin();
            }
        };

        listList.setLayoutManager(new LinearLayoutManager(context));
        listList.setAdapter(adapter);
    }

}
