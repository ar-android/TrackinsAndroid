package com.ahmadrosid.inspections.fragment_view.fragment.login;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.SettingsActivity;
import com.ahmadrosid.inspections.activity.view.LoginView;
import com.ahmadrosid.inspections.data.data.UserProfile;
import com.ahmadrosid.inspections.fragment_view.presenter.login.FragmentUserSetImagePresenter;
import com.ahmadrosid.inspections.ui.dialog.DialogPickImage;
import com.ahmadrosid.inspections.utils.PermissionHelper;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class FragmentSetImage extends Fragment {

    public static final int CAMERA_REQUEST_IMG_BANNER = 1;
    public static final int GALLERY_REQUEST_IMG_BANNER = 2;
    public static final int CAMERA_REQUEST_IMG_PROFILE = 3;
    public static final int GALLERY_REQUEST_IMG_PROFILE = 4;

    private LoginView loginView;
    private FragmentUserSetImagePresenter presenter;
    private PermissionHelper permission;

    @BindView(R.id.img_banner) ImageView img_banner;
    @BindView(R.id.img_profile) ImageView img_profile;
    @BindView(R.id.username) TextView username;
    @BindView(R.id.email) TextView email;

    public FragmentSetImage(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_set_user, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupPresenter();
        setupView();
    }

    private void setupView() {
        UserProfile profile = UserProfile.getInstance(getContext()).getUserProfile();
        if (profile != null) {
            username.setText(profile.username);
            email.setText(profile.email);
        }
    }

    private void setupPresenter() {
        presenter = new FragmentUserSetImagePresenter(this, s -> Snackbar.make(getView(), s, Snackbar.LENGTH_SHORT).show());
        permission = new PermissionHelper(getActivity());
    }

    @OnClick(R.id.change_img_banner) void changeImgBanner() {
        DialogPickImage
                .getInstance(getContext())
                .setCallback(new DialogPickImage.ClickDialog() {
                    @Override public void clickCamera() {
                        pickImageFromCamera(CAMERA_REQUEST_IMG_BANNER);

                    }

                    @Override public void clickGallery() {
                        pickImageFromGallery(GALLERY_REQUEST_IMG_BANNER);
                    }
                }).show();
    }

    @OnClick(R.id.change_img_profile) void changeImgProfile() {
        DialogPickImage
                .getInstance(getContext())
                .setCallback(new DialogPickImage.ClickDialog() {
                    @Override public void clickCamera() {
                        pickImageFromCamera(CAMERA_REQUEST_IMG_PROFILE);
                    }

                    @Override public void clickGallery() {
                        pickImageFromGallery(GALLERY_REQUEST_IMG_PROFILE);
                    }
                }).show();
    }

    @OnClick(R.id.done_set_img) void done() {
        if (presenter.commit()) {
            Intent i = new Intent(getContext(), SettingsActivity.class);
            startActivity(i);
            getActivity().finish();
        }
    }

    private void pickImageFromGallery(int requestCode) {
        if (permission.isSdkVersionM()) {
            loginView.requestPermissionGallery(() -> setOpenGallery(requestCode));
        } else {
            setOpenGallery(requestCode);
        }
    }

    private void setOpenGallery(int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode);
    }

    private void pickImageFromCamera(int setOpenGallery) {
        if (permission.isSdkVersionM()) {
            loginView.requestPermissionCamera(() -> setOpenCamera(setOpenGallery));
        } else {
            setOpenCamera(setOpenGallery);
        }
    }

    private void setOpenCamera(int setOpenGallery) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = presenter.createImageFile(setOpenGallery);
            if (photoFile != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, setOpenGallery);
            }
        }
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CAMERA_REQUEST_IMG_BANNER:
                    presenter.setImageFromCamera(img_banner);
                    break;
                case CAMERA_REQUEST_IMG_PROFILE:
                    presenter.setImageFromCamera(img_profile);
                    break;
                case GALLERY_REQUEST_IMG_BANNER:
                    presenter.setImgFromGallery(data, img_banner, GALLERY_REQUEST_IMG_BANNER);
                    break;
                case GALLERY_REQUEST_IMG_PROFILE:
                    presenter.setImgFromGallery(data, img_profile, GALLERY_REQUEST_IMG_PROFILE);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        this.loginView = null;
    }
}
