package com.ahmadrosid.inspections.fragment_view.view.report;

/**
 * Created by ocittwo on 10/10/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public interface PermissionImpl {
    void onPermissionGranted();
}
