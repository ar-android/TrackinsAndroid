package com.ahmadrosid.inspections.fragment_view.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.ReportActivity;
import com.ahmadrosid.inspections.helper.IntentAnim;
import com.ahmadrosid.inspections.helper.SetToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo on 10/4/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class FragmentHome extends Fragment {

    @BindView(R.id.btn_new_report) Button btn;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SetToolbar.setTitleFragment(this, "Home");
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupView();
    }

    private void setupView() {
        Typeface lato_light = Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Light.ttf");
        btn.setTypeface(lato_light);
    }

    @OnClick(R.id.menu_inspections) void clickInspection(){
        setFragment(new FragmentInspection());
    }

    @OnClick(R.id.menu_list) void clickList(){
        setFragment(new FragmentList());
    }

    @OnClick(R.id.menu_locations) void clickLocations(){
        setFragment(new FragmentLocation());
    }

    @OnClick(R.id.menu_report) void clickReport(){
        setFragment(new FragmentReport());
    }

    @OnClick(R.id.menu_notifications) void clickNotif(){
        setFragment(new FragmentNotifications());
    }

    @OnClick(R.id.menu_settings) void  clickSettings(){
        setFragment(new FragmentSettings());
    }

    @OnClick(R.id.btn_new_report) void clickNewReport(){
        Intent intent = new Intent(getContext(), ReportActivity.class);
        getActivity().startActivity(intent);
    }

    private void setFragment(final Fragment fragment){
        new Handler().postDelayed(() -> getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit(), 500);
    }
}
