package com.ahmadrosid.inspections.fragment_view.presenter.login;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.ahmadrosid.inspections.api.ApiBuilder;
import com.ahmadrosid.inspections.api.ApiServices;
import com.ahmadrosid.inspections.api.model.UserLogin;
import com.ahmadrosid.inspections.data.model.ResponseLogin;
import com.ahmadrosid.inspections.fragment_view.fragment.login.FragmentLogin;
import com.ahmadrosid.inspections.fragment_view.view.login.FragmentLoginView;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.SharedPref;
import com.ahmadrosid.inspections.ui.dialog.DialogShowMessages;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ahmadrosid.inspections.helper.Constants.HAVE_SETTINGS;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | All Rights Reserved
 */
public class FragmentLoginPresenter {
    private static final String TAG = "FragmentLoginPresenter";

    private final FragmentLoginView view;
    private final Context context;

    public FragmentLoginPresenter(FragmentLogin fragment) {
        this.view = fragment.getViews();
        this.context = fragment.getContext();
    }

    private boolean emailNotValid = false;

    public void login(String email, String password) {

        view.showProgress();
        String reg_email = SharedPref.getString(Constants.User.EMAIL);
        String reg_password = SharedPref.getString(Constants.User.PASSWORD);
        Log.d(TAG, "login: " + reg_email);

        if (validateInput(email, password)) {
            if (!Constants.isMatchEmail(email)) {
                emailNotValid = true;
                view.hideProgress();
                view.showMessage("Email not valid!");
            } else if (reg_email == null) {
                SharedPref.saveString(Constants.User.EMAIL, email);
                SharedPref.saveString(Constants.User.PASSWORD, password);
                DialogShowMessages.getInstance(context)
                        .setMessage("You not registered!\nPlease register!")
                        .setDismis(700)
                        .show();
                view.showProgress();
                new Handler().postDelayed(this::register, 700);
            } else if (!emailNotValid) {
                new Handler().postDelayed(() -> {
                    if (!email.contains(reg_email)) {
                        view.showMessage("Your email or password incorrect!");
                        view.hideProgress();
                    } else if (!password.contains(reg_password)) {
                        view.showMessage("Your email or password incorrect!");
                        view.hideProgress();
                    } else {
                        SharedPref.setDefaults("api_token", "jfs7sjsgdfogfdjk", context);
                        view.hideProgress();
                        view.successLogin();
                    }
                }, 2500);
            }else{
                view.hideProgress();
            }
        } else {
            view.hideProgress();
        }
    }

    private void register() {
        view.hideProgress();
        view.register();
    }

    private void checkLogin(String email, String password) {
        view.showProgress();
        UserLogin user = new UserLogin(email, password);
        ApiServices apiServices = ApiBuilder.call();
        apiServices.login(user).enqueue(new Callback<ResponseLogin>() {
            @Override public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if (response.body().isSuccess()) {
                    view.hideProgress();
                    view.successLogin();
                    SharedPref.setDefaults("api_token", response.body().api_token, context);
                } else {
                    view.hideProgress();
                    view.showMessage(response.body().message);
                }

                Log.d(TAG, "onResponse: " + response.body().toString());
            }

            @Override public void onFailure(Call<ResponseLogin> call, Throwable t) {

            }
        });
    }

    private boolean validateInput(String email, String password) {
        if (TextUtils.isEmpty(email)) {
            view.showMessage("Please input your email!");
            return false;
        } else if (TextUtils.isEmpty(password)) {
            view.showMessage("Please input your password!");
            return false;
        }
        return true;
    }

    public boolean isHaveSettings() {
        String have_settings = SharedPref.getDefaults(HAVE_SETTINGS, context);
        return !TextUtils.isEmpty(have_settings);
    }

}
