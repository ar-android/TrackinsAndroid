package com.ahmadrosid.inspections.fragment_view.presenter.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.ahmadrosid.inspections.data.data.UserProfile;
import com.ahmadrosid.inspections.fragment_view.fragment.login.FragmentSetImage;
import com.ahmadrosid.inspections.fragment_view.view.login.FragmentUserSetImageView;
import com.ahmadrosid.inspections.helper.BitmapHelper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.zelory.compressor.FileUtil;

import static com.ahmadrosid.inspections.fragment_view.fragment.login.FragmentSetImage.CAMERA_REQUEST_IMG_BANNER;
import static com.ahmadrosid.inspections.fragment_view.fragment.login.FragmentSetImage.CAMERA_REQUEST_IMG_PROFILE;
import static com.ahmadrosid.inspections.fragment_view.fragment.login.FragmentSetImage.GALLERY_REQUEST_IMG_BANNER;
import static com.ahmadrosid.inspections.fragment_view.fragment.login.FragmentSetImage.GALLERY_REQUEST_IMG_PROFILE;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class FragmentUserSetImagePresenter {
    private static final String TAG = "FragmentUserSetImagePre";

    private FragmentSetImage fragment;
    private final Context context;
    private final FragmentUserSetImageView view;

    private String mCurrentPhotoPath;
    private String img_banner_path;
    private String img_profile_path;

    public FragmentUserSetImagePresenter(FragmentSetImage fragmentSetImage, FragmentUserSetImageView view) {
        this.fragment = fragmentSetImage;
        this.context = fragmentSetImage.getContext();
        this.view = view;
    }

    public void setImgFromGallery(Intent data, ImageView img, int galleryRequestImg) {
        Uri imageUri = data.getData();
        try {
            File file = FileUtil.from(context, imageUri);
            File img_compress = BitmapHelper.compressImage(context, file);
            img.setImageBitmap(BitmapFactory.decodeFile(img_compress.getAbsolutePath()));

            switch (galleryRequestImg) {
                case GALLERY_REQUEST_IMG_BANNER:
                    img_banner_path = "file:" + img_compress.getPath();
                    Log.d(TAG, "setImgFromGallery: " + img_banner_path);
                    break;
                case GALLERY_REQUEST_IMG_PROFILE:
                    img_profile_path = "file:" + img_compress.getPath();
                    Log.d(TAG, "setImgFromGallery: " + img_profile_path);
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Log.d(TAG, "getPath: " + projection);
        @SuppressLint("Recycle") Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return uri.getPath();
    }

    public void setImageFromCamera(ImageView imageView) {
        try {
            Bitmap mImageBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(mCurrentPhotoPath));
            imageView.setImageBitmap(mImageBitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat") public File createImageFile(int REQUEST_CODE) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String imageFileName = "JIMG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,
                    ".jpeg",
                    storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert image != null;

        if (REQUEST_CODE == CAMERA_REQUEST_IMG_BANNER) {
            img_banner_path = "file:" + image.getAbsolutePath();
            Log.d(TAG, "createImageFile: " + img_banner_path);
        } else if (REQUEST_CODE == CAMERA_REQUEST_IMG_PROFILE) {
            img_profile_path = "file:" + image.getAbsolutePath();
            Log.d(TAG, "createImageFile: " + img_profile_path);
        }

        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public boolean commit() {
        if (TextUtils.isEmpty(img_profile_path)) {
            view.showMessages("Please set Your img profile!");
            return false;
        } else if (TextUtils.isEmpty(img_banner_path)) {
            new UserProfile(context)
                    .setPathImgProfile(img_profile_path)
                    .saveUserImage();
        } else {
            new UserProfile(context)
                    .setPathImgBanner(img_banner_path)
                    .setPathImgProfile(img_profile_path)
                    .saveUserImage();
        }

        return true;
    }
}
