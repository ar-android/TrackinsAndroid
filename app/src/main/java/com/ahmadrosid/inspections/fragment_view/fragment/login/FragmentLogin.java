package com.ahmadrosid.inspections.fragment_view.fragment.login;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.MainActivity;
import com.ahmadrosid.inspections.activity.SettingsActivity;
import com.ahmadrosid.inspections.activity.view.LoginView;
import com.ahmadrosid.inspections.fragment_view.presenter.login.FragmentLoginPresenter;
import com.ahmadrosid.inspections.fragment_view.view.login.FragmentLoginView;
import com.ahmadrosid.inspections.helper.ProgressLoader;
import com.ahmadrosid.inspections.ui.dialog.DialogShowMessages;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | All Rights Reserved
 */
public class FragmentLogin extends Fragment implements FragmentLoginView {

    private final LoginView loginView;
    private FragmentLoginPresenter presenter;
    private ProgressLoader loader;

    public FragmentLoginView getViews() {
        return this;
    }

    public FragmentLogin(LoginView loginView) {
        this.loginView = loginView;
    }

    @BindView(R.id.input_email) EditText input_email;
    @BindView(R.id.input_password) EditText input_password;
    @BindView(R.id.btn_login) Button btn_login;
    @BindView(R.id.register) Button btn_register;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupPresenter();
        setupView();
    }

    private void setupView() {
        Typeface lato_light = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Light.ttf");
        input_email.setTypeface(lato_light);
        input_password.setTypeface(lato_light);
        btn_login.setTypeface(lato_light);
        btn_register.setTypeface(lato_light);
    }

    private void setupPresenter() {
        presenter = new FragmentLoginPresenter(this);
        loader = new ProgressLoader(getContext());
    }

    @OnClick(R.id.btn_login) void clickLogin() {
        presenter.login(input_email.getText().toString(), input_password.getText().toString());
    }

    @OnClick(R.id.register) void clickRegister() {
        loginView.setActivityFragment(new FragmentRegister(loginView));
    }

    @Override public void showMessage(String s) {
        DialogShowMessages.getInstance(getContext())
                .setMessage(s)
                .setDismis(3000)
                .show();
    }

    @Override public void showProgress() {
        loader.showProgressDialog();
    }

    @Override public void hideProgress() {
        if(loader.isLoading())
        loader.hideProgressDialog();
    }

    @Override public void successLogin() {
        setIntent(presenter.isHaveSettings() ? MainActivity.class : SettingsActivity.class);
    }

    @Override public void register() {
        clickRegister();
    }

    private void setIntent(Class clazz){
        Intent i = new Intent(getContext(), clazz);
        startActivity(i);
        getActivity().finish();
    }
}
