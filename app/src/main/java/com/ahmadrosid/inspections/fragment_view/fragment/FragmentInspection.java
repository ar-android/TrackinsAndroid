package com.ahmadrosid.inspections.fragment_view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.ReportActivity;
import com.ahmadrosid.inspections.fragment_view.presenter.home.FragmentInspectionPresenter;
import com.ahmadrosid.inspections.fragment_view.view.home.FragmentInspectionView;
import com.ahmadrosid.inspections.helper.SetToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ocittwo on 02/10/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | App All Rights Reserved
 */
public class FragmentInspection extends Fragment implements FragmentInspectionView, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private FragmentInspectionPresenter presenter;

    @BindView(R.id.swipe_layout) SwipeRefreshLayout swipe_layout;
    @BindView(R.id.list_inspections) RecyclerView lis_inspections;
    @BindView(R.id.tx_nodata) TextView empty_inspections;
    @BindView(R.id.fab) FloatingActionButton fab;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SetToolbar.setTitleFragment(this, "Report Inspections");
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inspection, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupPresenter();
        fab.setOnClickListener(this);
        swipe_layout.setOnRefreshListener(this);
    }

    private void setupPresenter() {
        presenter = new FragmentInspectionPresenter(getContext(), this);
        presenter.loadData();
    }

    @Override public void setListInspections(RecyclerView.Adapter adapter) {
        if (adapter != null) {
            empty_inspections.setVisibility(View.GONE);
            lis_inspections.setAdapter(null);
            lis_inspections.setLayoutManager(new LinearLayoutManager(getContext()));
            lis_inspections.setAdapter(adapter);
        }
    }

    @Override public void showNoData() {
        empty_inspections.setVisibility(View.VISIBLE);
    }

    @Override public void finishRefresh() {
        swipe_layout.setRefreshing(false);
    }

    @Override public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                startActivity(new Intent(getContext(), ReportActivity.class));
                break;
        }
    }

    @Override public void onRefresh() {
        presenter.loadData();
    }
}
