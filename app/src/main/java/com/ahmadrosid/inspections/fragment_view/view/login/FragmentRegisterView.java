package com.ahmadrosid.inspections.fragment_view.view.login;

import com.ahmadrosid.inspections.core.Presenter;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | All Rights Reserved
 */
public interface FragmentRegisterView extends Presenter.View{
    void showMessage(String s);
    void successRegister();
}