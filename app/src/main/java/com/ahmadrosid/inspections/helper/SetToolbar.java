package com.ahmadrosid.inspections.helper;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;

import java.lang.reflect.Field;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | All Rights Reserved
 */
public class SetToolbar {

    private static final String TAG = "SetToolbar";

    /**
     * SetToolbar title fragment
     *
     * @param fragment Fragment
     * @param title String
     */
    public static void setTitleFragment(Fragment fragment, String title){
        ActionBar ab = ((AppCompatActivity)fragment.getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setTitle(title);
        }
    }

    /**
     * Set toolbar
     *
     * @param activity AppCompatActivity
     */
    public static void set(AppCompatActivity activity){
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
    }

    /**
     * Get toolbar
     *
     * @param activity AppCompatActivity
     * @return ActionBar
     */
    public static ActionBar get(AppCompatActivity activity){
        return activity.getSupportActionBar();
    }

    /**
     * Set back button
     *
     * @param activity AppCompactActivity
     */
    public static void setBack(AppCompatActivity activity) {
        ActionBar ab = activity.getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Set typeface toolbar serif
     *
     * @param toolbar Toolbar
     */
    public static void setToolbarTypefaceSerif(Toolbar toolbar) {
        TextView titleTextView = null;
        try {
            Field f = toolbar.getClass().getDeclaredField("mTitleTextView");
            f.setAccessible(true);
            titleTextView = (TextView) f.get(toolbar);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
            ignored.printStackTrace();
        }
        if (titleTextView != null) {
            Log.d(TAG, "setToolbarTypefaceSerif: toolbar have been set to serif");
            Typeface lato_light = Typeface.createFromAsset(toolbar.getContext().getAssets(),"fonts/Lato-Light.ttf");
            titleTextView.setTypeface(lato_light, Typeface.BOLD);
        }
    }

    /**
     * Initialize toolbar
     *
     * @param activity AppCompatActivity
     */
    public static void setSerifToolbar(AppCompatActivity activity) {
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        setToolbarTypefaceSerif(toolbar);
    }

    /**
     * Set icon back
     *
     * @param activity AppCompatActivity
     * @param icon Integer
     */
    public static void setIconBack(AppCompatActivity activity, int icon){
        ActionBar ab = activity.getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(icon);
        }
    }

}
