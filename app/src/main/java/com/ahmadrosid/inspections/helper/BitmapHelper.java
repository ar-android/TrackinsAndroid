package com.ahmadrosid.inspections.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;

import id.zelory.compressor.Compressor;

/**
 * Created by ocittwo on 02/09/16.
 * <p/>
 * Helper form managing image resizing and createUserProfile into file
 */
public class BitmapHelper {

    /**
     * Compress bitmap image from file
     *
     * @param context Context
     * @param actualImage File
     * @return File
     */
    public static File compressImage(Context context, File actualImage) {
        return new Compressor.Builder(context)
                .setMaxWidth(640)
                .setMaxHeight(480)
                .setQuality(75)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getAbsolutePath())
                .build()
                .compressToFile(actualImage);
    }
}
