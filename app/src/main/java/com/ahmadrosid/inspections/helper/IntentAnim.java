package com.ahmadrosid.inspections.helper;

import android.support.v7.app.AppCompatActivity;

import com.ahmadrosid.inspections.R;

/**
 * Created by ocittwo on 10/24/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class IntentAnim {

    public static void up(AppCompatActivity activity){
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public static void down(AppCompatActivity activity){
        activity.overridePendingTransition(R.anim.slide_down, R.anim.no_change);
    }
}
