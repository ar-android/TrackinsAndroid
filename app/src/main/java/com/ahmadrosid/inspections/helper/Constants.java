package com.ahmadrosid.inspections.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ocittwo on 02/09/16.
 */
public class Constants {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean isMatchEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }


    public static final String CREATED_AT = "created_at";

    /**
     * Intent sections
     */
    public static final int IMG_LEFT = 100;
    public static final int IMG_RIGHT = 200;

    public static final int LEFT_FRONT_IDLER_PHOTO = 300;
    public static final int LEFT_REAR_IDLER_PHOTO = 400;
    public static final int RIGHT_FRONT_IDLER_PHOTO = 500;
    public static final int RIGHT_REAR_IDLER_PHOTO = 600;
    public static final int LEFT_SIDE_TRACK_SHOE_PHOTO = 700;
    public static final int RIGHT_SIDE_TRACK_SHOE_PHOTO = 800;

    public static final int REQUEST_SET_MY_LOCATION = 900;
    public static final int LEFT_ONE_CARRIER_IMG = 1000;
    public static final int LEFT_TWO_CARRIER_IMG = 1001;
    public static final int LEFT_THREE_CARRIER_IMG = 1002;
    public static final int RIGHT_ONE_CARRIER_IMG = 1003;
    public static final int RIGHT_TWO_CARRIER_IMG = 1004;
    public static final int RIGHT_THREE_CARRIER_IMG = 1005;

    /**
     * Settings sections
     */
    public static final String HAVE_SETTINGS = "have settings";
    public static final String API_TOKEN = "api_token";

    public static final String IMG_PROFILE_PATH = "img_profile_path";
    public static final String IMG_BANNER_PATH = "img_banner_path";

    /**
     * Pick image left report
     */
    public static final int REQUEST_GALLERY = 0;
    public static final int REQUEST_CAMERA = 1;

    // Cache fragment report
    public static final String IMAGE_FRAGMENT_GENERAL = "path image general";
    public static final String JSON_GENERAL_SPINNER = "json general spinner";
    public static final String JSON_GENERAL_INPUT_FIELD = "json general input field";

    public static final String JSON_LEFT_INPUT_FIELD = "json left input field";
    public static final String JSON_LEFT_IMAGE_PATH = "json image path";
    public static final String JSON_LEFT_TEXT_HINT = "text hint field";

    public static final String JSON_RIGHT_INPUT_FIELD = "json right input field";
    public static final String JSON_RIGHT_IMAGE_PATH = "json right image path";
    public static final String JSON_RIGHT_TEXT_HINT = "text right hint field";

    public static final String SPINNER_FORM_GENERAL = "spinner general";
    public static final String INPUT_FIELD_GENERAL = "input field general";

    public class User{
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
    }

    public class RequestCode {
        public static final int CAMERA = 101;
        public static final int GALLERY = 102;
    }

    public class Maps {
        public static final String CURRENT_LAT_LON = "current_lat_lon";
        public static final String LAST_LAT_LON = "last_lat_lon";
    }

    public class Settings{
        public static final String ORGANIZATION = "organization";
        public static final String REGION = "region";
        public static final String DISTRICT= "district";
        public static final String DELAER_NAME= "dealer_name";
        public static final String DELAER_CODE= "dealer_code";
        public static final String SALES_NAME= "sales_name";
        public static final String IS_EDIT_MODE = "isEditMode";
    }
}
