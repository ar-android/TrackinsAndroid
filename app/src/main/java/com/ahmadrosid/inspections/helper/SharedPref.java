package com.ahmadrosid.inspections.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ahmadrosid.inspections.App;

/**
 * Created by ocittwo on 02/09/16.
 */
public class SharedPref {

    private static final String TAG = "SharedPref";

    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public static void deleteDefault(Context context, String api_token) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(api_token);
        editor.apply();
    }

    public static void saveString(String key, String value) {
        setDefaults(key, value, App.getContext());
    }

    public static String getString(String key) {
        return getDefaults(key, App.getContext());
    }

    public static void deleteString(String key){
        deleteDefault(App.getContext(), key);
    }
}
