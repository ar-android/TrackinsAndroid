package com.ahmadrosid.inspections.helper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.FileUtil;

/**
 * Created by ocittwo on 10/4/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class SetImg {

    private final Context context;

    public SetImg(Context context) {
        this.context = context;
    }

    public void set(Intent data, ImageView img) {
        Uri selectedImageUriLeft = data.getData();
        String selectedImagePathLeft = getPath(selectedImageUriLeft);
        try {
            Uri uri = Uri.parse("file://" + selectedImagePathLeft);
            File file = FileUtil.from(context, uri);
            File img_compress = BitmapHelper.compressImage(context, file);
            img.setImageBitmap(BitmapFactory.decodeFile(img_compress.getAbsolutePath()));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return uri.getPath();
    }

}
