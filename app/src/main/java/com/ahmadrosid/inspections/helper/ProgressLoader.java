package com.ahmadrosid.inspections.helper;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by ocittwo on 02/09/16.
 */
public class ProgressLoader {
    private ProgressDialog mProgressDialog;
    private Context context;

    public ProgressLoader(Context context){
        this.context = context;
    }

    /**
     * Show progress dialog
     */
    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
        }
        mProgressDialog.show();
    }

    /**
     * Hide progres dialog
     */
    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public boolean isLoading() {
        return mProgressDialog.isShowing();
    }
}
