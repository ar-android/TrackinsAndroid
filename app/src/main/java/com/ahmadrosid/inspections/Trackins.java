package com.ahmadrosid.inspections;

import android.app.Application;

/**
 * Created by ocittwo on 10/7/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class Trackins {

    private static Application APP_INSTANCE;

    public static void init(Application application, String qiscusAppId) {
        APP_INSTANCE = application;
    }

    public static Application getAppInstance() {
        return APP_INSTANCE;
    }
}
