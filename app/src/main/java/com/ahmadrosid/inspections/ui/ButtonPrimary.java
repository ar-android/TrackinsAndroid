package com.ahmadrosid.inspections.ui;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.Button;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.helper.TypefaceUtil;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class ButtonPrimary extends Button{

    public ButtonPrimary(Context context) {
        this(context, null);
    }

    public ButtonPrimary(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setBackground(ContextCompat.getDrawable(context, R.drawable.btn_primary));
        this.setTextColor(Color.WHITE);
        this.setTypeface(TypefaceUtil.get(context));
    }
}
