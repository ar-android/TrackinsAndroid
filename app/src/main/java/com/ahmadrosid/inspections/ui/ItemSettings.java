package com.ahmadrosid.inspections.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class ItemSettings extends LinearLayout{

    private TextView mHintText;
    private TextView mItemText;

    public ItemSettings(Context context) {
        this(context, null);
    }

    public ItemSettings(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_setting, this, true);

        TypedArray array = context.obtainStyledAttributes(attrs,
                R.styleable.ItemSettings, 0, 0);
        String hint = array.getString(R.styleable.ItemSettings_itemTextHint);

        array.recycle();

        mHintText = (TextView) findViewById(R.id.hint);
        mItemText = (TextView) findViewById(R.id.item);

        if (!(hint != null && hint.isEmpty())){
            mHintText.setText(hint);
            mItemText.setText(hint);
        }
    }

    public void setItem(String s){
        mItemText.setText(s);
    }
}
