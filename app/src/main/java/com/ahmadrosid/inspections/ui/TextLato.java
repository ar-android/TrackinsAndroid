package com.ahmadrosid.inspections.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by ocittwo on 10/4/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class TextLato extends TextView{

    public TextLato(Context context) {
        this(context, null);
    }

    public TextLato(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface lato_light = Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Light.ttf");
        this.setTypeface(lato_light);
    }
}
