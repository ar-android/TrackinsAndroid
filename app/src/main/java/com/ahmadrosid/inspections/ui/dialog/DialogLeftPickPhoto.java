package com.ahmadrosid.inspections.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.data.data.ReportLeftRight;
import com.ahmadrosid.inspections.helper.PickImageHelper;
import com.ahmadrosid.inspections.ui.FloatLabelTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class DialogLeftPickPhoto extends Dialog {
    private static final String TAG = "DialogLeftPickPhoto";


    public static final String GALLERY = "gallery";
    public static final String CAMERA = "camera";

    private final ReportLeftRight model;
    private final DialogLeftPickPhotoImpl pickPhoto;

    @BindView(R.id.tittle) TextView txt_tittle;
    @BindView(R.id.title_data_input) FloatLabelTextView title_data_input;
    @BindView(R.id.img) ImageView img;

    public DialogLeftPickPhoto(Context context, ReportLeftRight model, DialogLeftPickPhotoImpl pickPhoto) {
        super(context);
        this.model = model;
        this.pickPhoto = pickPhoto;
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_left_pick_photo);
        getWindow().getDecorView();
        ButterKnife.bind(this);
        setupView();
    }

    private void setupView() {
        txt_tittle.setText(model.title);
        title_data_input.setHint(model.title);
        if (model.value > 0)
            title_data_input.setText("" + model.value);
        if (model.path_photo != null)
            new PickImageHelper(getContext()).setImageFromPath(img, model.path_photo);
    }

    @OnClick(R.id.img) void pickImage() {
        DialogPickImage.getInstance(getContext())
                .setCallback(new DialogPickImage.ClickDialog() {
                    @Override public void clickCamera() {
                        pickPhoto.pickImageFrom(img, CAMERA);
                    }

                    @Override public void clickGallery() {
                        pickPhoto.pickImageFrom(img, GALLERY);
                    }
                }).show();
    }

    @OnClick(R.id.btn_click_ok) void clickOk() {
        if (TextUtils.isEmpty(title_data_input.getText().toString())) {
            showMessage("Please fill input " + model.title);

        } else {
            pickPhoto.done(title_data_input.getText().toString());
            dismiss();
        }
    }

    @OnClick(R.id.btn_click_cancel) void clickCancel() {
        dismiss();
    }

    private void showMessage(String s){
        DialogShowMessages.getInstance(getContext()).setMessage(s).show();
    }

    public interface DialogLeftPickPhotoImpl {
        void pickImageFrom(ImageView img, String from);
        void done(String value);
    }


}