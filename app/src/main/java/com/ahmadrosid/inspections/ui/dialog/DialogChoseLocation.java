package com.ahmadrosid.inspections.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.PickLocation;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.SharedPref;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ocittwo on 10/17/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class DialogChoseLocation extends Dialog{
    public static DialogChoseLocation instance;

    public static DialogChoseLocation getInstance(Context context) {
        instance = new DialogChoseLocation(context);
        return instance;
    }

    public DialogChoseLocation(Context context) {
        super(context);
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_location);
        getWindow().getDecorView();
        ButterKnife.bind(this);
    }

    @OnClick(R.id.current_location) void current(){
        String lastLocation = SharedPref.getDefaults(Constants.Maps.LAST_LAT_LON, getContext());
        String currentLocation = SharedPref.getDefaults(Constants.Maps.CURRENT_LAT_LON, getContext());
        if (currentLocation == null && lastLocation == null){
            DialogShowMessages.getInstance(getContext())
                    .setMessage("Failed get las location\nPlease select current location!")
                    .show();
        }
        new Handler().postDelayed(this::dismiss, 1500);
    }

    @OnClick(R.id.pick_location) void pick(){
        Intent intent = new Intent(getContext(), PickLocation.class);
        getContext().startActivity(intent);
        new Handler().postDelayed(this::dismiss, 1500);
    }

}
