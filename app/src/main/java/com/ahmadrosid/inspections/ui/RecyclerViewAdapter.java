package com.ahmadrosid.inspections.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by ocittwo on 31/08/16.
 */
public abstract class RecyclerViewAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private List<T> data;
    private Class<T> model;
    private int layout;
    private Class<VH> holder;

    public RecyclerViewAdapter(List<T> data, Class<T> model, int layout, Class<VH> holder) {
        this.data = data;
        this.model = model;
        this.layout = layout;
        this.holder = holder;
    }

    @Override public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        try {
            Constructor<VH> constructor = holder.getConstructor(View.class);
            return constructor.newInstance(view);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void onBindViewHolder(VH holder, int position) {
        T model = getItem(position);
        executeViewHolder(holder, model, position);
    }

    abstract protected void executeViewHolder(VH viewHolder, T model, int position);

    private T getItem(int position) {
        return data.get(position);
    }

    @Override public int getItemCount() {
        return data.size();
    }
}
