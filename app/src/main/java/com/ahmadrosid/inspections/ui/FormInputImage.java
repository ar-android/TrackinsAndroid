package com.ahmadrosid.inspections.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.helper.PickImageHelper;

/**
 * Created by ocittwo on 10/9/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class FormInputImage extends LinearLayout {

    private boolean inputFormatNumber;
    private FloatLabelTextView mFloatLabelTextView;

    private ImageView mImageView;
    private String hint;
    private String imgPath;

    private boolean isRequired;
    private boolean isNoImages;

    public FormInputImage(Context context) {
        this(context, null);
    }

    public FormInputImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.START);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_report_left_right, this, true);

        mFloatLabelTextView = (FloatLabelTextView) findViewById(R.id.input_title);
        mImageView = (ImageView) findViewById(R.id.img_item);

        TypedArray array = context.obtainStyledAttributes(attrs,
                R.styleable.FloatLabelTextView, 0, 0);
        hint = array.getString(R.styleable.FloatLabelTextView_editText_hint);
        isRequired = array.getBoolean(R.styleable.FloatLabelTextView_isRequired, true);
        isNoImages = array.getBoolean(R.styleable.FloatLabelTextView_isNoImages, false);
        inputFormatNumber = array.getBoolean(R.styleable.FloatLabelTextView_editText_input_number, true);
        array.recycle();

        if (!TextUtils.isEmpty(hint))
            mFloatLabelTextView.setHint(hint);

        if (isNoImages) {
            mImageView.setVisibility(GONE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mFloatLabelTextView.getLayoutParams();
            layoutParams.rightMargin = 10;
            mFloatLabelTextView.setLayoutParams(layoutParams);
        }

        if (!inputFormatNumber)
            mFloatLabelTextView.getmEditText().setInputType(InputType.TYPE_CLASS_TEXT);

    }

    public String getHint() {
        return hint;
    }

    public String getText() {
        return mFloatLabelTextView.getText().toString();
    }

    public void setText(String text) {
        mFloatLabelTextView.getmEditText().setText(text);
    }

    public void setImage(String imgPath) {
        new PickImageHelper(getContext()).setImageFromPath(mImageView, imgPath);
    }

    public ImageView getImageView() {
        return mImageView;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getImgPath() {
        return imgPath;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public boolean isNoImages() {
        return isNoImages;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }
}
