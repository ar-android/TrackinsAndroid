package com.ahmadrosid.inspections.api.model;

/**
 * Created by ocittwo on 01/09/16.
 */
public class PostInspections {

    public String title;
    public String picture;
    public String company;
    public String description;

    public PostInspections(String title, String picture, String company, String description) {
        this.title = title;
        this.picture = picture;
        this.company = company;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getPicture() {
        return picture;
    }

    public String getCompany() {
        return company;
    }

    public String getDescription() {
        return description;
    }
}
