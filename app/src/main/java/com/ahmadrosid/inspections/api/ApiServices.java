package com.ahmadrosid.inspections.api;

import com.ahmadrosid.inspections.api.model.ResponseInspections;
import com.ahmadrosid.inspections.api.model.ResponseRequest;
import com.ahmadrosid.inspections.api.model.UserLogin;
import com.ahmadrosid.inspections.data.model.ResponseLogin;
import com.ahmadrosid.inspections.fragment_view.model.User;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | All Rights Reserved
 */
public interface ApiServices {

    /**
     * Register User
     *
     * @param user User
     */
    @POST("api/v1/register") Call<ResponseRequest> register(@Body User user);

    /**
     * Login User
     *
     * @param user User
     */
    @POST("api/v1/login") Call<ResponseLogin> login(@Body UserLogin user);

    /**
     * Get list inspections
     */
    @GET("api/v1/inspections") Call<ResponseInspections> getInspections();

    /**
     * Post multipart data
     *
     * @param title String
     * @param company String
     * @param description String
     * @param pictures File
     */
    @Multipart @POST("api/inspections/new")
    Call<ResponseRequest> create(
            @Part("title") RequestBody title,
            @Part("company") RequestBody company,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part pictures
    );


}
