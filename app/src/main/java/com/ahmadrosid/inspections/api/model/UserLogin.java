package com.ahmadrosid.inspections.api.model;

/**
 * Created by ocittwo on 10/3/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class UserLogin {
    public String email;
    public String password;

    public UserLogin(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
