package com.ahmadrosid.inspections.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.ahmadrosid.inspections.R;

/**
 * Created by ocittwo on 19/09/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | App All Rights Reserved
 n */
public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        new Handler().postDelayed(() -> startActivity(new Intent(TestActivity.this, ReportActivity.class)), 2500);

    }

    private void setFragment(Fragment fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, fragment, null)
                .commit();
    }

}
