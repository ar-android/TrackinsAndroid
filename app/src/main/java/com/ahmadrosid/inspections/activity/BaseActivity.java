package com.ahmadrosid.inspections.activity;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by ocittwo on 10/24/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class BaseActivity extends AppCompatActivity{
}
