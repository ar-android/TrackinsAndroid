package com.ahmadrosid.inspections.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.data.data.UserProfile;
import com.ahmadrosid.inspections.data.local.TrackinsCacheManager;
import com.ahmadrosid.inspections.fragment_view.fragment.FragmentHome;
import com.ahmadrosid.inspections.fragment_view.fragment.FragmentInspection;
import com.ahmadrosid.inspections.fragment_view.fragment.FragmentList;
import com.ahmadrosid.inspections.fragment_view.fragment.FragmentLocation;
import com.ahmadrosid.inspections.fragment_view.fragment.FragmentNotifications;
import com.ahmadrosid.inspections.fragment_view.fragment.FragmentSettings;
import com.ahmadrosid.inspections.helper.SetToolbar;
import com.ahmadrosid.inspections.helper.SharedPref;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.IOException;

import static com.ahmadrosid.inspections.helper.Constants.API_TOKEN;
import static com.ahmadrosid.inspections.helper.Constants.HAVE_SETTINGS;

/**
 * Created by ocittwo.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | All Rights Reserved
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    private Toolbar toolbar;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SetToolbar.setToolbarTypefaceSerif(toolbar);
        setupView();
    }

    /**
     * SetToolbar active fragment
     *
     * @param fragment Fragment
     */
    private void setActiveFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private void setupView() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setActiveFragment(new FragmentHome());
        setHeaderContent(navigationView);
        new TrackinsCacheManager(this).clearCacheFragment();
    }

    /**
     * Set slider and profile header
     */
    private void setHeaderContent(NavigationView navigationView) {
        final View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
        ImageView image_banner = (ImageView) headerLayout.findViewById(R.id.img_banner);
        CircularImageView image_profile = (CircularImageView) headerLayout.findViewById(R.id.img_profile);
        TextView user_name = (TextView) headerLayout.findViewById(R.id.user_name);
        TextView email = (TextView) headerLayout.findViewById(R.id.email);

        UserProfile profile = new UserProfile(this).getUserProfile();
        if (profile != null) {
            user_name.setText(profile.username);
            email.setText(profile.email);
            if (profile.path_img_profile == null) {
                image_profile.setImageResource(R.drawable.blank_profile);
            } else {
                setImage(image_profile, profile.path_img_profile);
            }
            if (profile.path_img_banner != null) {
                setImage(image_banner, profile.path_img_banner);
            } else {
                image_banner.setImageResource(R.drawable.bg_banner_slider);
            }

            Log.d(TAG, "setHeaderContent: " + profile.path_img_profile);
        }
    }

    public void setImage(ImageView imageView, String path) {
        Bitmap mImageBitmap = null;
        try {
            mImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageView.setImageBitmap(mImageBitmap);
    }

    @Override public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                new Handler().postDelayed(() -> {
                    startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                    finish();
                }, 1000);
                break;
            case R.id.action_logout:
                logOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:
                setActiveFragment(new FragmentHome());
                break;
            case R.id.nav_inspection:
                setActiveFragment(new FragmentInspection());
                break;
            case R.id.nav_location:
                setActiveFragment(new FragmentLocation());
                break;
            case R.id.nav_list:
                setActiveFragment(new FragmentList());
                break;
            case R.id.nav_notification:
                setActiveFragment(new FragmentNotifications());
                break;
            case R.id.nav_settings:
                setActiveFragment(new FragmentSettings());
                break;
            case R.id.nav_log_out:
                logOut();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logOut() {
        SharedPref.deleteDefault(getApplicationContext(), API_TOKEN);
        SharedPref.deleteDefault(getApplicationContext(), HAVE_SETTINGS);
        Intent logout = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(logout);
        finish();
    }
}
