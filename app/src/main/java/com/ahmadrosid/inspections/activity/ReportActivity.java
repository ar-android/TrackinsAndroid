package com.ahmadrosid.inspections.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.view.ReportActivityView;
import com.ahmadrosid.inspections.fragment_view.fragment.report.ReportFragmentGeneral;
import com.ahmadrosid.inspections.fragment_view.fragment.report.ReportFragmentLeft;
import com.ahmadrosid.inspections.fragment_view.view.report.PermissionImpl;
import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.SetToolbar;
import com.ahmadrosid.inspections.helper.SharedPref;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by ocittwo on 28/09/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | App All Rights Reserved
 */
public class ReportActivity extends AppCompatActivity implements ReportActivityView {

    private ActionBar actionBar;

    private String[] permsCamera = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private String[] permsGallery = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private String[] permsLocation = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private final int REQUEST_PERMISSION = 1;

    public static final int REQUEST_PERMISSION_CAMERA = 100;
    public static final int REQUEST_PERMISSION_GALLERY = 200;

    private PermissionImpl permissions;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        SetToolbar.setBack(this);
        SetToolbar.setIconBack(this, R.drawable.ic_arrow_back_white);

        if (savedInstanceState == null)
            setActiveFragment(new ReportFragmentGeneral());

        actionBar = getSupportActionBar();

        String currentLocation = SharedPref.getDefaults(Constants.Maps.CURRENT_LAT_LON, this);
        if (currentLocation == null) {
            getLastLocation();
        }
    }

    private void setActiveFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container_report, fragment)
                .commit();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private static final String TAG = "ReportActivity";

    @Override public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount() - 1;
        if (count > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
        setTitles();
    }

    private void setTitles() {
        Log.d(TAG, "setTitles: opened!");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container_report);
        if (f instanceof ReportFragmentLeft) {
            SetToolbar.get(this).setTitle("LEFT");
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case REQUEST_PERMISSION_CAMERA:
                boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean cameraWriteFile = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                if (cameraAccepted && cameraWriteFile) permissionGranted();
                break;
            case REQUEST_PERMISSION_GALLERY:
                boolean galleryAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (galleryAccepted) permissionGranted();
                break;
        }

    }

    private void permissionGranted() {
        permissions.onPermissionGranted();
    }

    @Override public void requestPermissionCamera(PermissionImpl permissions) {
        this.permissions = permissions;
        requestPermissions(permsCamera, REQUEST_PERMISSION_CAMERA);
    }

    @Override public void requestPermissionGallery(PermissionImpl permissions) {
        this.permissions = permissions;
        requestPermissions(permsGallery, REQUEST_PERMISSION_GALLERY);
    }

    @Override public void setTitleToolbar(String title) {
        actionBar.setTitle(title);
    }

    private void getLastLocation() {
        String lastLocation = SharedPref.getDefaults(Constants.Maps.LAST_LAT_LON, this);
        if (lastLocation == null) {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                            @Override public void onConnected(@Nullable Bundle bundle) {
                                setLasLocation();
                            }

                            @Override public void onConnectionSuspended(int i) {
                                Log.d(TAG, "onConnectionSuspended: executed!");
                            }
                        })
                        .addOnConnectionFailedListener(connectionResult -> Log.d(TAG, "getLastLocation: failed find last location"))
                        .addApi(LocationServices.API)
                        .build();
                mGoogleApiClient.connect();
            }
        }
    }

    @Override protected void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void setLasLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(permsLocation, REQUEST_PERMISSION);
        } else {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                SharedPref.setDefaults(Constants.Maps.LAST_LAT_LON, "" + mLastLocation.getLatitude() + ", " + mLastLocation.getLongitude(), this);
                Log.d(TAG, "setLasLocation: "+ mLastLocation.getLatitude() + ", " + mLastLocation.getLongitude());
            }
        }
    }


}
