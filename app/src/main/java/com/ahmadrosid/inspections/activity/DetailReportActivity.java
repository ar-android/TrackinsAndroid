package com.ahmadrosid.inspections.activity;

import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.data.Database;
import com.ahmadrosid.inspections.data.model.DBModelInspections;
import com.ahmadrosid.inspections.helper.Constants;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ocittwo on 19/09/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | App All Rights Reserved
 */
public class DetailReportActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.image) ImageView image;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.company) TextView company;
    @BindView(R.id.description) TextView description;
    @BindView(R.id.created_at) TextView created_at;
    @BindView(R.id.btn_back) Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_report);
        ButterKnife.bind(this);
        try {
            setupView();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setup view report inspections
     */
    private void setupView() throws java.text.ParseException {
        /**
         * Get data from database
         */
        DBModelInspections data = loadData();
        String date = getDate(data.created_at);

        /**
         * SetToolbar toolbar
         */
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(data.title);
        getSupportActionBar().setSubtitle(date);

        /**
         * SetToolbar view content
         */
        Glide.with(this).load(data.picture_path).into(image);
        title.setText(data.title);
        company.setText(data.company);
        description.setText(data.description);
        created_at.setText(date);
        btn_back.setOnClickListener(this);
    }

    /**
     * Load data from local database by created_at
     *
     * @return DBModelInspections
     */
    private DBModelInspections loadData() {
        String created_at = getIntent().getStringExtra(Constants.CREATED_AT);
        Database database = new Database(this);
        SQLiteDatabase db = database.getWritableDatabase();
        return database.getLocalDataByDate(db, created_at);
    }

    @Override public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_back:
                onBackPressed();
                finish();
                break;
        }
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Get date from SimpleDateFormat
     *
     * @param s String simple date format
     * @return String
     * @throws ParseException
     * @throws java.text.ParseException
     */
    public static String getDate(String s) throws ParseException, java.text.ParseException {
        Date date = new SimpleDateFormat("ddMMyyyy_HHmm").parse(s);
        return new SimpleDateFormat("EE MM MMM, hh:mm").format(date);
    }
}
