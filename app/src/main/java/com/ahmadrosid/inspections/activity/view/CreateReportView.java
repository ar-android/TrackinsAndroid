package com.ahmadrosid.inspections.activity.view;

/**
 * Created by ocittwo on 01/09/16.
 */
public interface CreateReportView {
    void showUnvalidate(String s);
    void closeAndBack();
    void createReportSuccess(boolean isUploadedToServer);
}
