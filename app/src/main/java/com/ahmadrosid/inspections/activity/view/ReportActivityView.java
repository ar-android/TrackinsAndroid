package com.ahmadrosid.inspections.activity.view;

import com.ahmadrosid.inspections.core.Presenter;
import com.ahmadrosid.inspections.fragment_view.view.report.PermissionImpl;

/**
 * Created by ocittwo on 10/10/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public interface ReportActivityView extends Presenter.View{
    void requestPermissionCamera(PermissionImpl permissions);
    void requestPermissionGallery(PermissionImpl permissions);
    void setTitleToolbar(String title);
}
