package com.ahmadrosid.inspections.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.view.LoginView;
import com.ahmadrosid.inspections.fragment_view.fragment.login.FragmentLogin;
import com.ahmadrosid.inspections.fragment_view.view.report.PermissionImpl;
import com.ahmadrosid.inspections.helper.Constants;

/**
 * Created by ocittwo on 19/09/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 * <p>
 * © 2016 | App All Rights Reserved
 */
public class LoginActivity extends AppCompatActivity implements LoginView {

    private PermissionImpl permission;
    private String[] permsCamera = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private String[] permsGallery = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setActivityFragment(new FragmentLogin(this));
    }

    @Override public void setActivityFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_login, fragment)
                .commit();
    }

    @Override public void requestPermissionCamera(PermissionImpl permission) {
        this.permission = permission;
        requestPermissions(permsCamera, Constants.RequestCode.CAMERA);
    }

    @Override public void requestPermissionGallery(PermissionImpl permission) {
        this.permission = permission;
        requestPermissions(permsGallery, Constants.RequestCode.GALLERY);
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.RequestCode.CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permission.onPermissionGranted();
                }
                break;
            case Constants.RequestCode.GALLERY:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    permission.onPermissionGranted();
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        permission = null;
    }
}

