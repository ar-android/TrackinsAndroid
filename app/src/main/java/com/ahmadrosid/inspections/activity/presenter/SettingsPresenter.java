package com.ahmadrosid.inspections.activity.presenter;

import android.content.Context;

import com.ahmadrosid.inspections.activity.view.SettingsView;
import com.ahmadrosid.inspections.api.ApiBuilder;
import com.ahmadrosid.inspections.api.ApiServices;
import com.ahmadrosid.inspections.api.model.UserLogin;
import com.ahmadrosid.inspections.core.Presenter;
import com.ahmadrosid.inspections.data.model.ResponseLogin;
import com.ahmadrosid.inspections.helper.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class SettingsPresenter implements Presenter<SettingsView>{

    private SettingsView settingsView;
    private final Context context;

    public SettingsPresenter(SettingsView settingsView) {
        this.context = settingsView.getContext();
    }

    public void saveSettings() {
        if (settingsView.validateForm()){
            settingsView.successSaveSettings();
        }
    }

    private void checkLogin(String email, String password) {
        settingsView.showLoader();
        UserLogin user = new UserLogin(email, password);
        ApiServices apiServices = ApiBuilder.call();
        apiServices.login(user).enqueue(new Callback<ResponseLogin>() {
            @Override public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if (response.body().isSuccess()) {
                    settingsView.hideLoader();
                    settingsView.successSaveSettings();
                    SharedPref.setDefaults("api_token", response.body().api_token, context);
                } else {
                    settingsView.hideLoader();
                    settingsView.successSaveSettings();
                }
            }

            @Override public void onFailure(Call<ResponseLogin> call, Throwable t) {
                settingsView.hideLoader();
                settingsView.successSaveSettings();
            }
        });
    }

    @Override public void onAttachView(SettingsView view) {
        this.settingsView = view;
    }

    @Override public void onDetachView() {
        settingsView = null;
    }
}
