package com.ahmadrosid.inspections.activity.view;

import android.support.v4.app.Fragment;

import com.ahmadrosid.inspections.core.Presenter;
import com.ahmadrosid.inspections.fragment_view.view.report.PermissionImpl;

public interface LoginView extends Presenter.View{
    void setActivityFragment(Fragment fragment);
    void requestPermissionCamera(PermissionImpl permission);
    void requestPermissionGallery(PermissionImpl permission);
}
