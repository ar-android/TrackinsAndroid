package com.ahmadrosid.inspections.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.activity.presenter.CreateReportPresenter;
import com.ahmadrosid.inspections.activity.view.CreateReportView;
import com.ahmadrosid.inspections.helper.NotifHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateReportActivity extends AppCompatActivity implements CreateReportView, View.OnClickListener {

    private static final int SELECT_PICTURE = 1;
    private CreateReportPresenter presenter;
    private static String selectedImagePath;

    @BindView(R.id.main_view) CoordinatorLayout main_view;
    @BindView(R.id.img_report) ImageView img_report;
    @BindView(R.id.input_title) EditText title;
    @BindView(R.id.input_company) EditText company;
    @BindView(R.id.input_description) EditText description;
    @BindView(R.id.btn_save) Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_create_report);
        ButterKnife.bind(this);
        setupPresenter();
        setupView();
    }

    private void setupPresenter() {
        presenter = new CreateReportPresenter(this, this);
    }

    private void setupView() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        img_report.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        setFlagTextInput(title);
        setFlagTextInput(company);
        setFlagTextInput(description);
    }

    @Override public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_report:
                setPickupImage();
                break;
            case R.id.btn_save:
                presenter.saveReport(
                        selectedImagePath,
                        title.getText().toString(),
                        company.getText().toString(),
                        description.getText().toString()
                );
                break;
        }
    }

    /**
     * Pickup image from gallery
     */
    private void setPickupImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = presenter.getPath(selectedImageUri);
                img_report.setImageURI(selectedImageUri);
                Log.d(TAG, "onActivityResult: " + selectedImagePath);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void showUnvalidate(String s) {
        Snackbar.make(btn_save, s, Snackbar.LENGTH_SHORT).show();
    }

    @Override public void closeAndBack() {
        onBackPressed();
    }

    @Override public void createReportSuccess(boolean isUploadedToServer) {
        if (isUploadedToServer){
            NotifHelper.commonNotif(this, "Uploaded success!", "Report have been reported to server!");
        }else{
            NotifHelper.commonNotif(this, "Uploaded success!", "Report have save to local database!");
        }
        this.closeAndBack();
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void setFlagTextInput(EditText view) {
        view.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    private static final String TAG = "CreateReportActivity";

}
