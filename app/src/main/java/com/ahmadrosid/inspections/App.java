package com.ahmadrosid.inspections;

import android.support.multidex.MultiDexApplication;

import com.ahmadrosid.inspections.helper.TypefaceUtil;

/**
 * Created by ocittwo on 02/09/16.
 *
 * @Web https://ahmadrosid.com/
 * @Email ocittwo@gmail.com
 * @Github https:/github.com/ar-android
 * @Developer Ahmad Rosid
 *
 * © 2016 | App All Rights Reserved
 */
public class App extends MultiDexApplication{

    public static App instance;

    public static App getContext() {
        return instance;
    }

    @Override public void onCreate() {
        super.onCreate();
        instance = this;
        TypefaceUtil.overrideFont(this, "SERIF", "fonts/Lato-Light.ttf");
    }

}
