package com.ahmadrosid.inspections.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.inspections.R;
import com.ahmadrosid.inspections.data.model.DBModelInspections;
import com.ahmadrosid.inspections.fragment_view.presenter.home.FragmentInspectionPresenter;
import com.ahmadrosid.inspections.helper.NetworkHelper;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ocittwo on 01/09/16.
 */
public class HolderInspections extends RecyclerView.ViewHolder {

    @BindView(R.id.img_inspections) ImageView img;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.company) TextView company;
    @BindView(R.id.date) TextView date;
    @BindView(R.id.btn_publish) Button btn_publish;

    public HolderInspections(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(DBModelInspections model, FragmentInspectionPresenter presenter) {
        if (NetworkHelper.isNetworkConnected(itemView.getContext())) {
//            String url_img = "http://ahmadrosid.com:9000/uploads/inspections/" + model.picture;
            Glide.with(itemView.getContext()).load(model.picture).into(img);
        } else {
            Glide.with(itemView.getContext()).load(model.picture).into(img);
        }

        if (model.approved.equals("1")) {
            btn_publish.setVisibility(View.VISIBLE);
            if (NetworkHelper.isNetworkConnected(itemView.getContext())) {
                presenter.uploadReportToServer(model);
            }
        }

        title.setText(model.title);
        company.setText(model.company);

        /**
         * Fetch hours
         */
        try {
            String dates = getHours(model.created_at);
            date.setText(dates);
        } catch (ParseException e) {
            e.printStackTrace();
            date.setText(model.created_at);
        }
    }

    /**
     * Generate hours
     *
     * @param format String format date
     */
    private String getHours(String format) throws ParseException {
        Date old = new SimpleDateFormat("ddMMyyyy_HHmm").parse(format);
        Date now = new Date();
        long diff = now.getTime() - old.getTime();

        long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        long hours = TimeUnit.MILLISECONDS.toHours(diff);

        if (hours < 1) {
            return String.valueOf(minutes) + " minutes";
        } else if (minutes < 60) {
            return String.valueOf(seconds) + " second";
        } else {
            return String.valueOf(hours) + " hours";
        }

    }
}
