package com.ahmadrosid.inspections.data.local;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;

import com.ahmadrosid.inspections.data.model.TrackinsSetting;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ocittwo on 10/7/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class TrackinsDB {

    static final String DATABASE_NAME = "trackins.db";
    static final int DATABASE_VERSION = 1;

    @SuppressLint("SimpleDateFormat") public static String getCreatedAt(){
        return new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
    }

    static abstract class SettingsTable{
        static final String TABLE_NAME = "settings";

        static final String COLUMN_ID = "id";
        static final String COLUMN_UNIQUE_ID = "unique_id";
        static final String ORGANIZATION = "organization";
        static final String REGION = "region";
        static final String DISTRICT = "district";
        static final String DEALER_NAME = "dealer_name";
        static final String DEALER_CODE = "dealer_code";
        static final String SALES_NAME = "sales_name";
        static final String CREATED_AT = "created_at";


        static final String CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_ID + " INTEGER," +
                        COLUMN_UNIQUE_ID + " TEXT," +
                        ORGANIZATION + " TEXT," +
                        REGION + " TEXT," +
                        DISTRICT + " TEXT," +
                        DEALER_NAME + " TEXT," +
                        DEALER_CODE + " TEXT," +
                        SALES_NAME + " TEXT," +
                        CREATED_AT + " TEXT," +
                        " ); ";

        static ContentValues toContentValues(TrackinsSetting trackinsSetting) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, trackinsSetting.getId());
            values.put(COLUMN_UNIQUE_ID, trackinsSetting.getOrganization());
            values.put(ORGANIZATION, trackinsSetting.getOrganization());
            values.put(REGION, trackinsSetting.getRegion());
            values.put(DISTRICT, trackinsSetting.getDistrict());
            values.put(DEALER_NAME, trackinsSetting.getDealer_name());
            values.put(SALES_NAME, trackinsSetting.getDealer_code());
            values.put(CREATED_AT, trackinsSetting.getCreated_at());
            return values;
        }


        static TrackinsSetting parseCursor(Cursor cursor) {
            TrackinsSetting trackinsSetting = new TrackinsSetting();
            trackinsSetting.setId(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_ID)));
            trackinsSetting.setOrganization(cursor.getString(cursor.getColumnIndexOrThrow(ORGANIZATION)));
            trackinsSetting.setRegion(cursor.getString(cursor.getColumnIndexOrThrow(REGION)));
            trackinsSetting.setDistrict(cursor.getString(cursor.getColumnIndexOrThrow(DISTRICT)));
            trackinsSetting.setDealer_name(cursor.getString(cursor.getColumnIndexOrThrow(DEALER_NAME)));
            trackinsSetting.setDealer_code(cursor.getString(cursor.getColumnIndexOrThrow(DEALER_CODE)));
            trackinsSetting.setSales_name(cursor.getString(cursor.getColumnIndexOrThrow(SALES_NAME)));
            trackinsSetting.setCreated_at(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_AT)));
            return trackinsSetting;
        }
    }
}
