package com.ahmadrosid.inspections.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class TrackinsSetting implements Parcelable{

    public long id;
    public String unique_id;
    public String organization;
    public String region;
    public String district;
    public String dealer_name;
    public String dealer_code;
    public String sales_name;
    public String created_at;

    public TrackinsSetting() {
    }

    protected TrackinsSetting(Parcel in) {
        id = in.readLong();
        unique_id = in.readString();
        organization = in.readString();
        region = in.readString();
        district = in.readString();
        dealer_name = in.readString();
        dealer_code = in.readString();
        sales_name = in.readString();
        created_at = in.readString();
    }

    public static final Creator<TrackinsSetting> CREATOR = new Creator<TrackinsSetting>() {
        @Override
        public TrackinsSetting createFromParcel(Parcel in) {
            return new TrackinsSetting(in);
        }

        @Override
        public TrackinsSetting[] newArray(int size) {
            return new TrackinsSetting[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }

    public String getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
    }

    public String getSales_name() {
        return sales_name;
    }

    public void setSales_name(String sales_name) {
        this.sales_name = sales_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(unique_id);
        dest.writeString(organization);
        dest.writeString(region);
        dest.writeString(district);
        dest.writeString(dealer_name);
        dest.writeString(dealer_code);
        dest.writeString(sales_name);
        dest.writeString(created_at);
    }
}
