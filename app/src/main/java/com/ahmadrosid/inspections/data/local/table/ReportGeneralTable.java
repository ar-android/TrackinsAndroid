package com.ahmadrosid.inspections.data.local.table;

import android.content.ContentValues;

import com.ahmadrosid.inspections.data.model.TrackinsReportGeneral;

/**
 * Created by ocittwo on 10/7/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public class ReportGeneralTable {

    static final String TABLE_NAME = "report_general_table";

    static final String COLUMN_ID = "id";
    static final String COLUMN_UNIQUE_ID = "unique_id";
    static final String COLUMN_CUSTOMER_NAME = "customer_name";
    static final String COLUMN_CUSTOMER_NUMBER = "customer_number";
    static final String COLUMN_JOB_SITE = "job_site";
    static final String COLUMN_GPS_COORDINATE = "gps_coordinate";
    static final String COLUMN_PHOTO_PATH = "photo_path";
    static final String COLUMN_FAMILY = "family";
    static final String COLUMN_MODEL_CONFIG = "model_config";
    static final String COLUMN_MODEL = "model";
    static final String COLUMN_EQUIPMENT_NUMBER = "equipment_number";
    static final String COLUMN_INSPECTIONS_DATE = "inspections_date";
    static final String COLUMN_HOUR_METER_READING = "hour_meter_reading";
    static final String COLUMN_HOUR_METER_PER_WEEK = "hour_meter_per_week";
    static final String COLUMN_UNDERFOOT_IMPACT = "underfoot_impact";
    static final String COLUMN_UNDERFOOT_ABRASIVE = "underfoot_abrasive";
    static final String COLUMN_UNDERFOOT_MOISTURE = "underfoot_moisture";
    static final String COLUMN_UNDERFOOT_PACKING = "underfoot_packing";
    static final String COLUMN_BUSHING_ALLOWABLE_WEAR = "bushing_allowable_wear";
    static final String COLUMN_LINK_ALLOWABLE_WEAR = "link_allowable_wear";
    static final String COLUMN_BUSHINGS_TURNED = "bushings_turned";
    static final String COLUMN_NOTES = "notes";
    static final String COLUMN_SHOE_TYPE = "shoe_type";
    static final String COLUMN_UNIT_OF_MEASURE = "unit_of_measure";
    static final String COLUMN_TRACK_ROLLER_QUANTITIES_SF = "track_roller_quantities_sf";
    static final String COLUMN_TRACK_ROLLER_QUANTITIES_DF = "track_roller_quantities_df";
    static final String COLUMN_TRACK_GROUP_PN = "track_group_pn";
    static final String COLUMN_LINK_ASSEMBLY_PN = "link_assembly_pn";
    static final String COLUMN_TRACK_SHOE_PN = "track_shoe_pn";
    static final String COLUMN_MASTER_SHOE_PN = "master_shoe_pn";
    static final String COLUMN_CARRIER_ROLLER_PN = "carrier_roller_pn";
    static final String COLUMN_FRONT_IDLER_PN = "front_idler_pn";
    static final String COLUMN_REAR_IDLER_PN = "rear_idler_pn";
    static final String COLUMN_SINGLE_FLANGE_PN = "single_flange_pn";
    static final String COLUMN_DOUBLE_FLANGE_PN = "double_flange_pn";
    static final String COLUMN_FIRST_SPROCKET_SEGMENT_PN = "first_sprocket_segment_pn";
    static final String COLUMN_SECONDS_SPROCKET_SEGMENT_PN = "seconds_sprocket_segment_pn";
    static final String CREATED_AT = "created_at";

    static final String CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_ID + " INTEGER, " +
                    COLUMN_UNIQUE_ID + " TEXT," +
                    COLUMN_CUSTOMER_NAME + " TEXT," +
                    COLUMN_CUSTOMER_NUMBER + " TEXT," +
                    COLUMN_JOB_SITE + " TEXT," +
                    COLUMN_GPS_COORDINATE + " TEXT," +
                    COLUMN_PHOTO_PATH + " TEXT," +
                    COLUMN_FAMILY + " TEXT," +
                    COLUMN_MODEL_CONFIG + " TEXT," +
                    COLUMN_MODEL + " TEXT, " +
                    COLUMN_EQUIPMENT_NUMBER + " TEXT," +
                    COLUMN_INSPECTIONS_DATE + " TEXT," +
                    COLUMN_HOUR_METER_READING + " TEXT," +
                    COLUMN_HOUR_METER_PER_WEEK + " TEXT," +
                    COLUMN_UNDERFOOT_IMPACT + " TEXT," +
                    COLUMN_UNDERFOOT_ABRASIVE + " TEXT," +
                    COLUMN_UNDERFOOT_MOISTURE + " TEXT," +
                    COLUMN_UNDERFOOT_PACKING + " TEXT," +
                    COLUMN_BUSHING_ALLOWABLE_WEAR + " TEXT," +
                    COLUMN_LINK_ALLOWABLE_WEAR + " TEXT," +
                    COLUMN_BUSHINGS_TURNED + " TEXT," +
                    COLUMN_NOTES + " TEXT," +
                    COLUMN_SHOE_TYPE + " TEXT," +
                    COLUMN_UNIT_OF_MEASURE + " TEXT," +
                    COLUMN_TRACK_ROLLER_QUANTITIES_SF + " TEXT," +
                    COLUMN_TRACK_ROLLER_QUANTITIES_DF + " TEXT," +
                    COLUMN_TRACK_GROUP_PN + " TEXT," +
                    COLUMN_LINK_ASSEMBLY_PN + " TEXT," +
                    COLUMN_TRACK_SHOE_PN + " TEXT," +
                    COLUMN_MASTER_SHOE_PN + " TEXT," +
                    COLUMN_CARRIER_ROLLER_PN + " TEXT," +
                    COLUMN_FRONT_IDLER_PN + " TEXT," +
                    COLUMN_REAR_IDLER_PN + " TEXT," +
                    COLUMN_SINGLE_FLANGE_PN + " TEXT," +
                    COLUMN_DOUBLE_FLANGE_PN + " TEXT," +
                    COLUMN_FIRST_SPROCKET_SEGMENT_PN + " TEXT," +
                    COLUMN_SECONDS_SPROCKET_SEGMENT_PN + " TEXT," +
                    CREATED_AT + " TEXT," +
                    " ); ";

    static ContentValues toContentValues(TrackinsReportGeneral trackinsReportGeneral){
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, trackinsReportGeneral.getId());
        values.put(COLUMN_UNIQUE_ID, trackinsReportGeneral.getUnique_id());
        values.put(CREATED_AT, trackinsReportGeneral.getCreated_at());
        return values;
    }
}
