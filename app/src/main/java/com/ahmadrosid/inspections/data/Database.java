package com.ahmadrosid.inspections.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ahmadrosid.inspections.data.model.DBModelInspections;

import java.util.ArrayList;

import static com.ahmadrosid.inspections.helper.constants.DatabaseConstants.DEALER_CODE;
import static com.ahmadrosid.inspections.helper.constants.DatabaseConstants.DEALER_NAME;
import static com.ahmadrosid.inspections.helper.constants.DatabaseConstants.DISTRICT;
import static com.ahmadrosid.inspections.helper.constants.DatabaseConstants.ORGANIZATION;
import static com.ahmadrosid.inspections.helper.constants.DatabaseConstants.REGION;
import static com.ahmadrosid.inspections.helper.constants.DatabaseConstants.SALES_NAME;
import static com.ahmadrosid.inspections.helper.constants.DatabaseConstants.TABLE_DATA_SETTING_LOCAL;

/**
 * Created by ocittwo on 03/09/16.
 *
 * Managing database SQLite
 */
public class Database extends SQLiteOpenHelper {

    /**
     * Common database sets
     */
    public static final String DATABASE_NAME = "inspections.db";
    public static final int VERSION = 1;

    /**
     * Database Table
     */
    public static final String TABLE_NAME_LOCAL = "inspections";
    public static final String TABLE_NAME_SERVER = "inspections_servers";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String PICTURE = "picture";
    public static final String COMPANY = "company";
    public static final String DESCRIPTION = "description";
    public static final String APPROVED = "approved";
    public static final String PICTURE_PATH = "picture_path";
    public static final String CREATED_AT = "created_at";

    public static final String TEXT = " TEXT, ";
    private static final String[] TABLES = {TITLE, PICTURE, COMPANY, DESCRIPTION, APPROVED, PICTURE_PATH, CREATED_AT};

    public Database(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createLocalTable(sqLiteDatabase);
    }

    /**
     * Create database table for local database
     *
     * @param sqLiteDatabase SQLiteDatabase
     */
    private void createLocalTable(SQLiteDatabase sqLiteDatabase) {
        final String QUERY_CREATE_DATABASE = "CREATE TABLE if not exists " +
                TABLE_NAME_LOCAL +
                " ( " +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TABLE_NAME_LOCAL + TEXT +
                TITLE + TEXT +
                PICTURE + TEXT +
                COMPANY + TEXT +
                DESCRIPTION + TEXT +
                APPROVED + TEXT +
                PICTURE_PATH + TEXT +
                CREATED_AT + " TEXT" +
                ");";
        sqLiteDatabase.execSQL(QUERY_CREATE_DATABASE);
    }

    /**
     * Create database for database from server
     *
     * @param db SQLiteDatabase
     */
    private void createTableServer(SQLiteDatabase db) {
        final String QUERY_CREATE_DATABASE_SERVERS = "CREATE TABLE if not exists " +
                TABLE_NAME_SERVER +
                " ( " +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TABLE_NAME_LOCAL + TEXT +
                TITLE + TEXT +
                PICTURE + TEXT +
                COMPANY + TEXT +
                DESCRIPTION + TEXT +
                APPROVED + TEXT +
                PICTURE_PATH + TEXT +
                CREATED_AT + " TEXT" +
                ");";
        db.execSQL(QUERY_CREATE_DATABASE_SERVERS);
    }

    /**
     * Create database for database from server
     *
     * @param db SQLiteDatabase
     */
    private void createTableDataSettings(SQLiteDatabase db) {
        final String QUERY_CREATE_DATABASE_SERVERS = "CREATE TABLE if not exists " +
                TABLE_DATA_SETTING_LOCAL +
                " ( " +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ORGANIZATION + TEXT +
                REGION + TEXT +
                DISTRICT + TEXT +
                DEALER_NAME + TEXT +
                DEALER_CODE + TEXT +
                SALES_NAME + TEXT +
                CREATED_AT + " TEXT" +
                ");";
        db.execSQL(QUERY_CREATE_DATABASE_SERVERS);
    }

    @Override public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }

    /**
     * Query insert database
     *
     * @param db   SQLiteDatabase
     * @param data DBModelInspections
     */
    public void insert(SQLiteDatabase db, DBModelInspections data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, data.title);
        contentValues.put(PICTURE, data.picture);
        contentValues.put(COMPANY, data.company);
        contentValues.put(DESCRIPTION, data.description);
        contentValues.put(APPROVED, data.approved);
        contentValues.put(PICTURE_PATH, data.picture_path);
        contentValues.put(CREATED_AT, data.created_at);
        db.insert(TABLE_NAME_LOCAL, null, contentValues);
    }

    /**
     * Query insert database from data that have fetch from server
     *
     * @param db   SQLiteDatabase
     * @param data DBModelInspections
     */
    public void insertFromServer(SQLiteDatabase db, DBModelInspections data) {
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_NAME_SERVER + "'");
        createTableServer(db);
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, data.title);
        contentValues.put(PICTURE, data.picture);
        contentValues.put(COMPANY, data.company);
        contentValues.put(DESCRIPTION, data.description);
        contentValues.put(APPROVED, data.approved);
        contentValues.put(PICTURE_PATH, data.picture_path);
        contentValues.put(CREATED_AT, data.created_at);
        db.insert(TABLE_NAME_SERVER, null, contentValues);
    }

    /**
     * Change Approved to 0
     *
     * @param db         SQLiteDatabase
     * @param created_at String
     */
    public void editApproved(SQLiteDatabase db, String created_at) {
        ContentValues cv = new ContentValues();
        cv.put(APPROVED, "0");
        db.update(TABLE_NAME_LOCAL, cv, CREATED_AT + " = '" + created_at + "' ", null);
    }

    /**
     * Get data from local database
     *
     * @param db SQLiteDatabase
     * @return DBModelInspections
     */
    public DBModelInspections getLocalDataByDate(SQLiteDatabase db, String created_at) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_LOCAL + " WHERE created_at = '" + created_at + "' ", null);
        DBModelInspections data = null;
        if (cursor != null && cursor.moveToFirst()) {
            data = new DBModelInspections(
                    0, getString(cursor, TITLE),
                    getString(cursor, PICTURE),
                    getString(cursor, COMPANY),
                    getString(cursor, DESCRIPTION),
                    getString(cursor, APPROVED),
                    getString(cursor, PICTURE_PATH),
                    getString(cursor, CREATED_AT)
            );
            cursor.close();
        }

        return data;
    }

    /**
     * Get data from local database
     *
     * @param db SQLiteDatabase
     * @param id long
     * @return DBModelInspections
     */
    public DBModelInspections getLocalServerDataById(SQLiteDatabase db, long id) {
        Cursor cursorc = db.rawQuery("SELECT * FROM " + TABLE_NAME_SERVER + " WHERE id = " + id, null);
        DBModelInspections data = new DBModelInspections(
                id, getString(cursorc, TITLE),
                getString(cursorc, PICTURE),
                getString(cursorc, COMPANY),
                getString(cursorc, DESCRIPTION),
                getString(cursorc, APPROVED),
                getString(cursorc, PICTURE_PATH),
                getString(cursorc, CREATED_AT)
        );

        return data;
    }

    /**
     * Get data inspections and store to ArrayList
     *
     * @param db SQLiteDatabase
     * @return ArrayList
     */
    public ArrayList<DBModelInspections> getAll(SQLiteDatabase db) {
        ArrayList<DBModelInspections> data = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME_LOCAL, TABLES, null, null, null, null, null);

        if (cursor == null)
            return null;

        if (cursor.moveToFirst()) {
            do {
                DBModelInspections inspections = new DBModelInspections(
                        cursor.getLong(cursor.getColumnIndex(ID) + 1),
                        getString(cursor, TITLE),
                        getString(cursor, PICTURE),
                        getString(cursor, COMPANY),
                        getString(cursor, DESCRIPTION),
                        getString(cursor, APPROVED),
                        getString(cursor, PICTURE_PATH),
                        getString(cursor, CREATED_AT)
                );
                data.add(inspections);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    /**
     * Get data inspections and store to ArrayList
     *
     * @param db SQLiteDatabase
     * @return ArrayList
     */
    public ArrayList<DBModelInspections> getAllDataFromServer(SQLiteDatabase db) {
        ArrayList<DBModelInspections> data = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME_SERVER, TABLES, null, null, null, null, null);

        if (cursor == null)
            return null;

        if (cursor.moveToFirst()) {
            do {
                DBModelInspections insepections = new DBModelInspections(
                        cursor.getLong(cursor.getColumnIndex(ID)),
                        getString(cursor, TITLE),
                        getString(cursor, PICTURE),
                        getString(cursor, COMPANY),
                        getString(cursor, DESCRIPTION),
                        getString(cursor, APPROVED),
                        getString(cursor, PICTURE_PATH),
                        getString(cursor, CREATED_AT)
                );
                data.add(insepections);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    /**
     * Get String from cursor database
     *
     * @param cursor Cursor
     * @param s      String name column
     * @return String
     */
    private String getString(Cursor cursor, String s) {
        return cursor.getString(cursor.getColumnIndex(s));
    }
}
