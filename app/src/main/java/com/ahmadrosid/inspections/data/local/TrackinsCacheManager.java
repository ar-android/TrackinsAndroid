package com.ahmadrosid.inspections.data.local;

import android.content.Context;
import android.util.Log;
import android.widget.Spinner;

import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.SharedPref;
import com.ahmadrosid.inspections.ui.FloatLabelTextView;
import com.ahmadrosid.inspections.ui.FormInputImage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ocittwo on 10/11/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class TrackinsCacheManager {

    private Context context;
    private Gson gson;

    public TrackinsCacheManager(Context context) {
        this.context = context;
        gson = new Gson();
    }

    public void cacheFragmentReportGeneral(ArrayList<Spinner> arraySpinner, ArrayList<FloatLabelTextView> arrayInputField) {
        ArrayList<Integer> dataSpinner = new ArrayList<>();
        ArrayList<String> dataInputField = new ArrayList<>();

        for (int i = 0; i < arraySpinner.size(); i++)
            dataSpinner.add(arraySpinner.get(i).getSelectedItemPosition());
        for (int i = 0; i < arrayInputField.size(); i++)
            dataInputField.add(arrayInputField.get(i).getText().toString());

        String jsonSpinner = gson.toJson(dataSpinner);
        String jsonInputField = gson.toJson(dataInputField);

        SharedPref.setDefaults(Constants.JSON_GENERAL_SPINNER, jsonSpinner, context);
        SharedPref.setDefaults(Constants.JSON_GENERAL_INPUT_FIELD, jsonInputField, context);
    }

    public Map<Object, List<String>> getCacheFragmentReportGeneral() {
        String jsonSpinner = SharedPref.getDefaults(Constants.JSON_GENERAL_SPINNER, context);
        String jsonInputField = SharedPref.getDefaults(Constants.JSON_GENERAL_INPUT_FIELD, context);

        if (jsonSpinner != null) {
            Type listType = new TypeToken<List<String>>() {}.getType();
            List<String> dataArrSpinner = gson.fromJson(jsonSpinner, listType);
            List<String> dataArrInputField = gson.fromJson(jsonInputField, listType);

            Map<Object, List<String>> data = new HashMap<>();
            data.put(Constants.JSON_GENERAL_SPINNER, dataArrSpinner);
            data.put(Constants.JSON_GENERAL_INPUT_FIELD, dataArrInputField);

            return data;
        }else{
            return null;
        }
    }

    public void cacheFragmentReportLeft(ArrayList<FormInputImage> arrayForm) {
        List<String> dataField = new ArrayList<>();
        List<String> dataHint = new ArrayList<>();
        List<String> dataImage = new ArrayList<>();

        for (FormInputImage formInputImage : arrayForm) {
            dataField.add(formInputImage.getText());
            dataHint.add(formInputImage.getHint());
            String imgPath = formInputImage.getImgPath();
            if (imgPath != null) {
                dataImage.add(formInputImage.getImgPath());
            }else{
                dataImage.add(null);
            }
        }

        String jsonDataField = gson.toJson(dataField);
        String jsonDataImage = gson.toJson(dataImage);
        String jsonDataHint = gson.toJson(dataHint);

        SharedPref.setDefaults(Constants.JSON_LEFT_INPUT_FIELD, jsonDataField, context);
        SharedPref.setDefaults(Constants.JSON_LEFT_IMAGE_PATH, jsonDataImage, context);
        SharedPref.setDefaults(Constants.JSON_LEFT_TEXT_HINT, jsonDataHint, context);
    }

    public Map<Object, List<String>> getCacheFragmentReportLeft(){
        String jsonDataField = SharedPref.getDefaults(Constants.JSON_LEFT_INPUT_FIELD, context);
        String jsonDataImage = SharedPref.getDefaults(Constants.JSON_LEFT_IMAGE_PATH, context);
        String jsonDataHint = SharedPref.getDefaults(Constants.JSON_LEFT_TEXT_HINT, context);
        Type listType = new TypeToken<List<String>>() {}.getType();
        if (jsonDataField != null){
            List<String> listField = gson.fromJson(jsonDataField, listType);
            List<String> listImage = gson.fromJson(jsonDataImage, listType);
            List<String> listHint = gson.fromJson(jsonDataHint, listType);

            Map<Object, List<String>> data  = new HashMap<>();
            data.put(Constants.JSON_LEFT_INPUT_FIELD, listField);
            data.put(Constants.JSON_LEFT_IMAGE_PATH, listImage);
            data.put(Constants.JSON_LEFT_TEXT_HINT, listHint);

            return data;
        }else {
            return null;
        }
    }

    public void cacheFragmentReportRight(ArrayList<FormInputImage> arrayForm) {
        List<String> dataField = new ArrayList<>();
        List<String> dataHint = new ArrayList<>();
        List<String> dataImage = new ArrayList<>();
        for (FormInputImage formInputImage : arrayForm) {
            dataField.add(formInputImage.getText());
            dataHint.add(formInputImage.getHint());
            String imgPath = formInputImage.getImgPath();
            if (imgPath != null) {
                dataImage.add(formInputImage.getImgPath());
            }else{
                dataImage.add(null);
            }
        }
        String jsonDataField = gson.toJson(dataField);
        String jsonDataImage = gson.toJson(dataImage);
        String jsonDataHint = gson.toJson(dataHint);

        SharedPref.setDefaults(Constants.JSON_RIGHT_INPUT_FIELD, jsonDataField, context);
        SharedPref.setDefaults(Constants.JSON_RIGHT_IMAGE_PATH, jsonDataImage, context);
        SharedPref.setDefaults(Constants.JSON_RIGHT_TEXT_HINT, jsonDataHint, context);
    }

    public Map<Object, List<String>> getCacheFragmentReportRight(){
        String jsonDataField = SharedPref.getDefaults(Constants.JSON_RIGHT_INPUT_FIELD, context);
        String jsonDataImage = SharedPref.getDefaults(Constants.JSON_RIGHT_IMAGE_PATH, context);
        String jsonDataHint = SharedPref.getDefaults(Constants.JSON_RIGHT_TEXT_HINT, context);
        Type listType = new TypeToken<List<String>>() {}.getType();
        if (jsonDataField != null){
            List<String> listField = gson.fromJson(jsonDataField, listType);
            List<String> listImage = gson.fromJson(jsonDataImage, listType);
            List<String> listHint = gson.fromJson(jsonDataHint, listType);

            Map<Object, List<String>> data  = new HashMap<>();
            data.put(Constants.JSON_RIGHT_INPUT_FIELD, listField);
            data.put(Constants.JSON_RIGHT_IMAGE_PATH, listImage);
            data.put(Constants.JSON_RIGHT_TEXT_HINT, listHint);

            return data;
        }else {
            return null;
        }
    }

    public void clearCacheFragment(){
        deleteCache(Constants.JSON_GENERAL_INPUT_FIELD);
        deleteCache(Constants.JSON_GENERAL_SPINNER);
        deleteCache(Constants.IMAGE_FRAGMENT_GENERAL);
        deleteCache(Constants.JSON_LEFT_INPUT_FIELD);
        deleteCache(Constants.JSON_LEFT_IMAGE_PATH);
        deleteCache(Constants.JSON_LEFT_TEXT_HINT);
        deleteCache(Constants.JSON_RIGHT_INPUT_FIELD);
        deleteCache(Constants.JSON_RIGHT_IMAGE_PATH);
        deleteCache(Constants.JSON_RIGHT_TEXT_HINT);
    }

    private void deleteCache(String key){
        SharedPref.deleteDefault(context, key);
    }

}
