package com.ahmadrosid.inspections.data.data;

/**
 * Created by ocittwo on 10/9/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class ReportLeftRight {
    public String title;
    public String path_photo;
    public int value;

    public ReportLeftRight(String title, String path_photo, int value) {
        this.title = title;
        this.path_photo = path_photo;
        this.value = value;
    }

    public ReportLeftRight(String s) {
        this.title = s;
    }

    @Override public String toString() {
        return "Reportleft data :" +
                "\n" + title +
                "\n" + path_photo +
                "\n" + value;
    }
}
