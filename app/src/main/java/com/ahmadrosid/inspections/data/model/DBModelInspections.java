package com.ahmadrosid.inspections.data.model;

/**
 * Created by ocittwo on 03/09/16.
 */
public class DBModelInspections {

    public long id;
    public String title;
    public String picture;
    public String company;
    public String description;
    public String approved;
    public String picture_path;
    public String created_at;

    public DBModelInspections(long id, String title, String picture, String company, String description, String approved, String picture_path, String created_at) {
        this.id = id;
        this.title = title;
        this.picture = picture;
        this.company = company;
        this.description = description;
        this.approved = approved;
        this.picture_path = picture_path;
        this.created_at = created_at;
    }

    public DBModelInspections(String title, String picture, String company, String description, String approved, String picture_path, String created_at) {
        this.title = title;
        this.picture = picture;
        this.company = company;
        this.description = description;
        this.approved = approved;
        this.picture_path = picture_path;
        this.created_at = created_at;
    }
}
