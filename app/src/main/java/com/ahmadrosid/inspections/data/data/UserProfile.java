package com.ahmadrosid.inspections.data.data;

import android.content.Context;
import android.text.TextUtils;

import com.ahmadrosid.inspections.helper.SharedPref;

/**
 * Created by ocittwo on 10/8/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class UserProfile {

    public static final String EMAIL = "email";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public static final String PATH_IMG_BANNER = "path_img_banner";
    public static final String PATH_IMG_PROFILE = "path_image_profile";

    private final Context context;

    public String email;
    public String username;
    public String password;

    public String path_img_banner;
    public String path_img_profile;

    public static UserProfile userProfile;

    public static UserProfile getInstance(Context context){
        userProfile = new UserProfile(context);
        return userProfile;
    }

    public UserProfile(Context context) {
        this.context = context;
    }

    public UserProfile getUserProfile() {
        String sp_username = SharedPref.getDefaults(USERNAME, context);
        String sp_email = SharedPref.getDefaults(EMAIL, context);
        String sp_path_img_banner = SharedPref.getDefaults(PATH_IMG_BANNER, context);
        String sp_path_img_profile = SharedPref.getDefaults(PATH_IMG_PROFILE, context);
        if (TextUtils.isEmpty(sp_username)) {
            return null;
        }
        return new UserProfile(context)
                .setEmail(sp_email)
                .setUsername(sp_username)
                .setPathImgBanner(sp_path_img_banner)
                .setPathImgProfile(sp_path_img_profile);
    }

    public UserProfile createUserProfile(UserImpl user) {
        SharedPref.setDefaults(EMAIL, email, context);
        SharedPref.setDefaults(USERNAME, username, context);
        SharedPref.setDefaults(PASSWORD, password, context);
        user.registerUser(email, username, password);
        return this;
    }

    public UserProfile saveUserImage() {
        if (path_img_banner == null) {
            SharedPref.setDefaults(PATH_IMG_PROFILE, path_img_profile, context);
        }
        SharedPref.setDefaults(PATH_IMG_BANNER, path_img_banner, context);
        SharedPref.setDefaults(PATH_IMG_PROFILE, path_img_profile, context);
        return this;
    }

    public UserProfile setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserProfile setUsername(String username) {
        this.username = username;
        return this;
    }

    public UserProfile setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserProfile setPathImgBanner(String pathImgBanner) {
        this.path_img_banner = pathImgBanner;
        return this;
    }

    public UserProfile setPathImgProfile(String pathImgProfile) {
        this.path_img_profile = pathImgProfile;
        return this;
    }

    public interface UserImpl {
        void registerUser(String email, String username, String password);
    }
}
