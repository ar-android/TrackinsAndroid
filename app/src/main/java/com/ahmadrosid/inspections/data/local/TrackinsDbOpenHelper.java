package com.ahmadrosid.inspections.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ocittwo on 10/7/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class TrackinsDbOpenHelper extends SQLiteOpenHelper{

    TrackinsDbOpenHelper(Context context) {
        super(context, TrackinsDB.DATABASE_NAME, null, TrackinsDB.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(TrackinsDB.SettingsTable.CREATE);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        clearOldData(db);
        onCreate(db);
    }

    private void clearOldData(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TrackinsDB.SettingsTable.TABLE_NAME);
    }
}
