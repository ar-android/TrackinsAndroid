package com.ahmadrosid.inspections.data;

import com.ahmadrosid.inspections.helper.Constants;
import com.ahmadrosid.inspections.helper.SharedPref;

import static com.ahmadrosid.inspections.helper.Constants.HAVE_SETTINGS;

/**
 * Created by ocittwo on 10/6/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */

public enum DataSettings {
    INSTANCE;

    public static DataSettings getInstance() {
        return INSTANCE;
    }

    public String organization;
    public String region;
    public String district;
    public String dealer_name;
    public String dealer_code;
    public String sales_name;

    public DataSettings setOrganization(String organization) {
        this.organization = organization;
        return this;
    }

    public DataSettings setRegion(String region) {
        this.region = region;
        return this;
    }

    public DataSettings setDistrict(String district) {
        this.district = district;
        return this;
    }

    public DataSettings setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
        return this;
    }

    public DataSettings setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
        return this;
    }

    public DataSettings setSales_name(String sales_name) {
        this.sales_name = sales_name;
        return this;
    }

    public DataSettings save() {
        SharedPref.saveString(Constants.Settings.ORGANIZATION, organization);
        SharedPref.saveString(Constants.Settings.REGION, region);
        SharedPref.saveString(Constants.Settings.DISTRICT, district);
        SharedPref.saveString(Constants.Settings.DELAER_NAME, dealer_name);
        SharedPref.saveString(Constants.Settings.DELAER_CODE, dealer_code);
        SharedPref.saveString(Constants.Settings.SALES_NAME, sales_name);
        SharedPref.saveString(HAVE_SETTINGS, "true");
        return this;
    }

    public DataSettings get() {
        return DataSettings.getInstance()
                .setOrganization(SharedPref.getString(Constants.Settings.ORGANIZATION))
                .setRegion(SharedPref.getString(Constants.Settings.REGION))
                .setDistrict(SharedPref.getString(Constants.Settings.DISTRICT))
                .setDealer_name(SharedPref.getString(Constants.Settings.DELAER_NAME))
                .setDealer_code(SharedPref.getString(Constants.Settings.DELAER_CODE))
                .setSales_name(SharedPref.getString(Constants.Settings.SALES_NAME));
    }

}
